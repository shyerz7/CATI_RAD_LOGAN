@extends('layouts.app')


@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css">
<!--<link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">-->
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css">

<link rel="stylesheet" href="{{ asset('css/sweetalert2.css') }}">
<style>
        #fountainG{
            position:relative;
            width:234px;
            height:28px;
            margin:auto;
        }

        .fountainG{
            position:absolute;
            top:0;
            background-color:rgb(0,0,0);
            width:28px;
            height:28px;
            animation-name:bounce_fountainG;
                -o-animation-name:bounce_fountainG;
                -ms-animation-name:bounce_fountainG;
                -webkit-animation-name:bounce_fountainG;
                -moz-animation-name:bounce_fountainG;
            animation-duration:1.5s;
                -o-animation-duration:1.5s;
                -ms-animation-duration:1.5s;
                -webkit-animation-duration:1.5s;
                -moz-animation-duration:1.5s;
            animation-iteration-count:infinite;
                -o-animation-iteration-count:infinite;
                -ms-animation-iteration-count:infinite;
                -webkit-animation-iteration-count:infinite;
                -moz-animation-iteration-count:infinite;
            animation-direction:normal;
                -o-animation-direction:normal;
                -ms-animation-direction:normal;
                -webkit-animation-direction:normal;
                -moz-animation-direction:normal;
            transform:scale(.3);
                -o-transform:scale(.3);
                -ms-transform:scale(.3);
                -webkit-transform:scale(.3);
                -moz-transform:scale(.3);
            border-radius:19px;
                -o-border-radius:19px;
                -ms-border-radius:19px;
                -webkit-border-radius:19px;
                -moz-border-radius:19px;
        }

        #fountainG_1{
            left:0;
            animation-delay:0.6s;
                -o-animation-delay:0.6s;
                -ms-animation-delay:0.6s;
                -webkit-animation-delay:0.6s;
                -moz-animation-delay:0.6s;
        }

        #fountainG_2{
            left:29px;
            animation-delay:0.75s;
                -o-animation-delay:0.75s;
                -ms-animation-delay:0.75s;
                -webkit-animation-delay:0.75s;
                -moz-animation-delay:0.75s;
        }

        #fountainG_3{
            left:58px;
            animation-delay:0.9s;
                -o-animation-delay:0.9s;
                -ms-animation-delay:0.9s;
                -webkit-animation-delay:0.9s;
                -moz-animation-delay:0.9s;
        }

        #fountainG_4{
            left:88px;
            animation-delay:1.05s;
                -o-animation-delay:1.05s;
                -ms-animation-delay:1.05s;
                -webkit-animation-delay:1.05s;
                -moz-animation-delay:1.05s;
        }

        #fountainG_5{
            left:117px;
            animation-delay:1.2s;
                -o-animation-delay:1.2s;
                -ms-animation-delay:1.2s;
                -webkit-animation-delay:1.2s;
                -moz-animation-delay:1.2s;
        }

        #fountainG_6{
            left:146px;
            animation-delay:1.35s;
                -o-animation-delay:1.35s;
                -ms-animation-delay:1.35s;
                -webkit-animation-delay:1.35s;
                -moz-animation-delay:1.35s;
        }

        #fountainG_7{
            left:175px;
            animation-delay:1.5s;
                -o-animation-delay:1.5s;
                -ms-animation-delay:1.5s;
                -webkit-animation-delay:1.5s;
                -moz-animation-delay:1.5s;
        }

        #fountainG_8{
            left:205px;
            animation-delay:1.64s;
                -o-animation-delay:1.64s;
                -ms-animation-delay:1.64s;
                -webkit-animation-delay:1.64s;
                -moz-animation-delay:1.64s;
        }



        @keyframes bounce_fountainG{
            0%{
            transform:scale(1);
                background-color:rgb(0,0,0);
            }

            100%{
            transform:scale(.3);
                background-color:rgb(255,255,255);
            }
        }

        @-o-keyframes bounce_fountainG{
            0%{
            -o-transform:scale(1);
                background-color:rgb(0,0,0);
            }

            100%{
            -o-transform:scale(.3);
                background-color:rgb(255,255,255);
            }
        }

        @-ms-keyframes bounce_fountainG{
            0%{
            -ms-transform:scale(1);
                background-color:rgb(0,0,0);
            }

            100%{
            -ms-transform:scale(.3);
                background-color:rgb(255,255,255);
            }
        }

        @-webkit-keyframes bounce_fountainG{
            0%{
            -webkit-transform:scale(1);
                background-color:rgb(0,0,0);
            }

            100%{
            -webkit-transform:scale(.3);
                background-color:rgb(255,255,255);
            }
        }

        @-moz-keyframes bounce_fountainG{
            0%{
            -moz-transform:scale(1);
                background-color:rgb(0,0,0);
            }

            100%{
            -moz-transform:scale(.3);
                background-color:rgb(255,255,255);
            }
        }
    </style>

@endsection


@section('content')
<div class="container-fluid">

    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-body">
                    
                    <div class="form-inline">

                      <div class="form-group">
                        <label for="email">Sort By Status</label>
                        <select class="form-control" id="status_id" name="status_id">
                         <option value="">----</option>
                         <option value="1">Sukses Interview</option>
                         <option value="2">Menolak</option>
                         <option value="3">Nomor tidak terpasang</option>
                         <option value="4">Reshcedule</option>
                         <option value="5">Salah Sambung</option>
                         <option value="6">Setengah Wawancara</option>
                         <option value="7">Tidak ada Jawaban</option>
                         <option value="8">Tidak ada nada</option>
                         <option value="9">Tidak Aktif</option>
                         <option value="10">Tidak Lolos Screening</option>
                         <option value="11">Kuota Sudah Full</option>
                         {{-- <option value="99">Fresh List</option>
                         <option value="100">All Status</option> --}}
                        </select>
                      </div>
                      
                    </div>
                    <br> 
                    <div id="listResponden" >
                        <table id="tableListResp" class="table table-striped table-bordered" cellspacing="0" width="100%">
                              <thead>
                                  <tr>
                                      <th>No</th>
                                      <th>RespID</th>
                                      <th>First Name</th>
                                      <th>Last Name</th>
                                      <th>Lihat Rekaman</th>
                                  </tr>
                              </thead>

                              <tbody>

                              </tbody>
                        </table>
                        </div>

                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">List Rekaman</h4>
      </div>
      <div class="modal-body">

        {{-- loader --}}
        <div id="fountainG">
            <div id="fountainG_1" class="fountainG"></div>
            <div id="fountainG_2" class="fountainG"></div>
            <div id="fountainG_3" class="fountainG"></div>
            <div id="fountainG_4" class="fountainG"></div>
            <div id="fountainG_5" class="fountainG"></div>
            <div id="fountainG_6" class="fountainG"></div>
            <div id="fountainG_7" class="fountainG"></div>
            <div id="fountainG_8" class="fountainG"></div>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

@endsection

@section('js')
<script src="{{ asset('js/jquery.blockUI.js') }}"></script>
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>
<!--<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>-->
<script src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script>
<script src="//cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>

<script>
  $(document).ready(function() {

    $('#tableListResp').DataTable();

    $("#status_id").on('change',function(e){
        var id_status    = e.target.value;
        var id_project   = {{ $project->id }};  

        $.ajax({
            url  : '{{ url('/client/ajaxListRespByStatus')}}/'+id_project+'/'+id_status,
            beforeSend: function(){
                $.blockUI(); 
            },
            type : 'get'
            ,
            success:function(html)
            {
                $('#listResponden').html(html);
                $('#tableListResp').DataTable();
                $.unblockUI(); 
                return false;
                // $(".qlist").show();
                // $(".loader").hide();
            }
        });

    });

  });
</script>
<script>
  function play(id) {
        // var id = $(this).attr('data-id');

        console.log(id);

        $("#myModal").modal('show');

        $.ajax({
          url   : "{!! route('listrekaman.playrekaman')  !!}", 
          type  : "GET",
          data:"id="+id,
          success:function(html) {
            $(".modal-body").html(html);
          } 
        });  
          
    }
</script>
@endsection
