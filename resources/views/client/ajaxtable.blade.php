<div id="listResponden" >
<table id="tableListResp" class="table table-striped table-bordered" cellspacing="0" width="100%">
      <thead>
          <tr>
              <th>No</th>
              <th>RespID</th>
              <th>First Name</th>
              <th>Status</th>
              <th>Lihat Rekaman</th>
          </tr>
      </thead>

      <tbody>
          <?php
            $no=1;
          ?>
          @foreach($respondens as $responden)

            <tr>
              <td>{{ $no }}</td>
              <td>{{ $responden->resp_id }}</td>
              <td>{{ $responden->first_name }}</td>
              <td>
                <span class="{{ $responden->status->label }}">
                  {{ $responden->status->name }}
                </span>
              </td>
              <td>
                
                @if(count($responden->rekaman))
                  <button class="btn btn-primary btn-xs" onclick="play('{!! $responden->id !!}')">
                    <i class="glyphicon glyphicon-play"></i> play
                  </button>
                @else
                  <span class="label label-warning">Belum diupload</span>
                @endif
                 

              </td>
            </tr>
            <?php $no++ ?>
          @endforeach


      </tbody>
</table>
</div>