@extends('layouts.app')


@section('content')
<div class="container-fluid">
    <div class="row">
        
        @include('backend.partials.sidebar')

        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">
                  Edit Responden
                </div>

                <div class="panel-body">
                    
                     {!!  Form::model($respondent, array('method' => 'put', 'route' => array('assignment.viewRespDetailUpdate',$respondent->id ), 'files'=> false,'class'=>'form-horizontal')) !!}

                        {{ csrf_field() }}


                        <div class="form-group{{ $errors->has('resp_id') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Kuota</label>

                            <div class="col-md-6">
                                <input id="resp_id" type="text" class="form-control" name="resp_id" value="{{ $respondent->kuota->nama_parameter }}" disabled="disabled">

                                @if ($errors->has('resp_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('resp_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('resp_id') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Kode Responden</label>

                            <div class="col-md-6">
                                <input id="resp_id" type="text" class="form-control" name="resp_id" value="{{ $respondent->resp_id }}" disabled="disabled">

                                @if ($errors->has('resp_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('resp_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('resp_id') ? ' has-error' : '' }}">
                            <label for="first_name" class="col-md-4 control-label">First Name</label>

                            <div class="col-md-6">
                                <input id="first_name" type="text" class="form-control" name="first_name" value="{{ $respondent->first_name }}"  disabled="disabled">

                                @if ($errors->has('first_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('resp_id') ? ' has-error' : '' }}">
                            <label for="last_name" class="col-md-4 control-label">Nomer HP</label>

                            <div class="col-md-6">
                                <input id="email" type="text" class="form-control" name="email" value="{{ $respondent->nomer_hp }}" disabled="disabled">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('resp_id') ? ' has-error' : '' }}">
                            <label for="last_name" class="col-md-4 control-label">Status Terakhir</label>

                            <div class="col-md-6">
                                <input id="email" type="text" class="form-control" name="email" value="{{ $respondent->status->name }}" disabled="disabled">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <!-- Select Basic -->
                          <div class="form-group">
                            <label class="col-md-4 control-label" for="status_id">Update Status Responden</label>
                            <div class="col-md-6">
                              {!! Form::select('status_id',$list_status,null, ['class' => 'form-control status_id']) !!}
                            </div>
                          </div>


                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Update
                                </button>

                            </div>
                        </div>
                    </form>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('js')
  
    <script src="{{ asset('js/clone-text.js') }}"></script>
  
@endsection



