@extends('layouts.app')

@section('css')
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">
<style>
     .dual-list .list-group {
            margin-top: 8px;
        }

        .list-left li, .list-right li {
            cursor: pointer;
        }

        .list-arrows {
            padding-top: 100px;
        }

            .list-arrows button {
                margin-bottom: 20px;
            }

        .list-respoden {
            height: 400px;
            overflow: scroll;
        }    
</style>

@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        
        @include('backend.partials.sidebar')

        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">
                  Panel Assignment Responden - <strong>{{ $agent->code }} - {{ $agent->name }}</strong>
                </div>

                <div class="panel-body">
                    
                    <div class="row">

                <div class="dual-list list-left col-md-5">
                    <div class="well text-right">
                        <div class="row">
                            <div class="col-md-10">
                                <div class="input-group">
                                    <span class="input-group-addon glyphicon glyphicon-search"></span>
                                    <input type="text" name="SearchDualList" class="form-control" placeholder="search" />
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="btn-group">
                                    <a class="btn btn-default " title="select all" id="selectAll"><i class="glyphicon glyphicon-unchecked"></i></a>
                                </div>
                            </div>
                        </div>
                        <form  method="POST" action="{{ route('assignment.assignProcess',[$agent->id,$kuota->id]) }}">
                         {{ csrf_field() }}
                        <ul class="list-group list-respoden checked-list-box">
            
                            @foreach($respondens as $responden)
                                <li class="list-group-item">
                                    <label><input type="checkbox" name="respondens[]" value="{{ $responden->id }}"> <strong> {{ $responden->first_name.' '.$responden->last_name }}</label>
                                </li>
                            @endforeach

                        </ul>
                         <button type="submit" class="btn btn-primary">Process Assignment</button>
                        </form>
                    </div>
                </div>

            </div>

                    
                </div>
            </div>
        </div>

    </div>
</div>
@endsection

@section('js')
<script>
    $(function () {
            
            $('#selectAll').click(function(e){
                e.preventDefault();

                $('input[type="checkbox"]').prop('checked',true).change();
            });

            $('body').on('click', '.list-group .list-group-item', function () {
                $(this).toggleClass('active');
            });

             $('.dual-list .selector').click(function () {
                var $checkBox = $(this);
                if (!$checkBox.hasClass('selected')) {
                   
                } else {
                   $('input[type="checkbox"]').prop('checked',false).change();
                }
            });

        });
</script>    

@endsection



