@extends('layouts.app')


@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="{{ asset('css/sweetalert2.css') }}">
<style>
  .widget {
    margin: 0 0 25px 0;
    display: block;
    -webkit-border-radius: 2px;
    -moz-border-radius: 2px;
    border-radius: 2px;
}
.widget .widget-heading {
    padding: 7px 15px;
    -webkit-border-radius: 2px 2px 0 0;
    -moz-border-radius: 2px 2px 0 0;
    border-radius: 2px 2px 0 0;
    text-transform: uppercase;
    text-align: center;
    background: #38BDFF;
    color: white;
}
.widget .widget-body {
    padding: 10px 15px;
    font-size: 36px;
    font-weight: 300;
    background: #8FDEFF;
}
</style>
@endsection


@section('content')
<div class="container-fluid">
    
    <div class="row">
        <div class="col-md-12">

          {{-- Untuk Pesan Sukses --}}
          @if(Session::has('alert-success'))
              <div class="alert alert-success">
                    {{ Session::get('alert-success') }}
                </div>
          @endif 

          @if(Session::has('alert-gagal'))
              <div class="alert alert-danger">
                    {{ Session::get('alert-gagal') }}
                </div>
          @endif

        </div>
      </div>

    <div class="row">
        
        @include('backend.partials.sidebar')

        <div class="col-md-9">

            <ol class="breadcrumb">
              <li><a href="{{ route('assignment.kuota') }}">Kuota</a></li>  
              <li class="#">{{ $kuota->nama_parameter }}</li>
              <li class="active">{{ $agent->name }}</li>
            </ol>

            <div class="panel panel-default">

                <div class="panel-body">
                    
                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Kuota</th>
                                <th>RespID</th>
                                <th>FirstName</th>
                                <th>Status</th>
                                <th>No Telpon</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        
                        <tbody>
                            
                            @foreach($respondens as $responden)
                                <tr>
                                    <td>{{ $responden->kuota->nama_parameter }}</td>
                                    <td>{{ $responden->resp_id }}</td>
                                    <td>{{ $responden->first_name }}</td>
                                    <td>
                                        <span class="{{ $responden->status->label }}">
                                            {{ $responden->status->name }}
                                        </span>
                                    </td>
                                    <td>{{ $responden->nomer_hp }}</td>
                                    
                                    <td>
                                        <div class="btn-group" role="group" aria-label="...">
                                          <a href="{{ route('assignment.viewRespDetail',[$agent->id,$kuota->id,$responden->id]) }}" class="btn btn-success">Rubah Status</a>
                                        </div>
                                    </td>
                                </tr>

                            @endforeach
                            
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>
<script src="{{ asset('js/sweetalert2.min.js') }}"></script>
<script src="{{ asset('js/confirm.js') }}"></script>
<script>
  $(document).ready(function() {
    $('#example').DataTable();
} );
</script>
@endsection

