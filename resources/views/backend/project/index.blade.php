@extends('layouts.app')


@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="{{ asset('css/sweetalert2.css') }}">
@endsection


@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="panel-heading">
                  List Project
                  <a href="{{ route('projects.create') }}" class="btn btn-primary">Tambah Project</a>
                </div>

                <div class="panel-body">
                    
                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Project</th>
                                <th>Target Responden</th>
                                <th>Client</th>
                                <th>Project Manager</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        
                        <tbody>
                            @foreach($projects as $project)
                              <tr>
                                  <td>{{ $project->name }}</td>
                                  <td>{{ $project->target_responden }}</td>
                                  <td>{{ $project->client_name }}</td>
                                  <td>{{ $project->project_manajer }}</td>
                                  <td>
                                    <div class="btn-group" role="group" aria-label="...">
                                      <a class="btn btn-primary">Show</a>
                                      <a href="{{ route('projects.edit',$project->id) }}" class="btn btn-success">Edit</a>
                                      <a href="#" data-target="{{ route('projects.destroy', [$project->id]) }}" class="btn btn-danger confirmation" onclick="hapusData(this)">Hapus</a>
                                    </div>
                                  </td>
                              </tr>
                            @endforeach
                            
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>
<script src="{{ asset('js/sweetalert2.min.js') }}"></script>
<script src="{{ asset('js/confirm.js') }}"></script>
<script>
  $(document).ready(function() {
    $('#example').DataTable();
} );
</script>
@endsection

