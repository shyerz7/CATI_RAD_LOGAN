@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Edit Project</div>
                <div class="panel-body">
                {!!  Form::model($project, array('method' => 'put', 'route' => array('projects.update', $project->id), 'files'=> false,'class'=>'form-horizontal')) !!}
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Nama</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ $project->name }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="target_responden" class="col-md-4 control-label">Target Responden</label>

                            <div class="col-md-6">
                                <input id="target_responden" type="text" class="form-control" name="target_responden" value="{{ $project->target_responden }}" required autofocus>

                                @if ($errors->has('target_responden'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('target_responden') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="client_name" class="col-md-4 control-label">Nama Client</label>

                            <div class="col-md-6">
                                <input id="client_name" type="text" class="form-control" name="client_name" value="{{ $project->client_name }}" required autofocus>

                                @if ($errors->has('client_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('client_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="project_manajer" class="col-md-4 control-label">Project Manajer</label>

                            <div class="col-md-6">
                                <input id="project_manajer" type="text" class="form-control" name="project_manajer" value="{{ $project->project_manajer }}" required autofocus>

                                @if ($errors->has('project_manajer'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('project_manajer') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>

                            </div>
                        </div>
                    
                {{ Form::close() }}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
