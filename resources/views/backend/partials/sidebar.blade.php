<div class="col-md-3 sidebar">
	<div class="list-group wizard-menu">
        <a href="{{ route('status.index') }}" class="list-group-item {{ Request::segment(3) == 'status' ? 'active' : '' }} step-1-menu" data-for=".step-1">
            <h4 class="list-group-item-heading">Status
                <span class="pull-right"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></span>
            </h4>
        </a>
        <a href="{{ route('kuotas.index') }}" class="list-group-item {{ Request::segment(3) == 'kuotas' ? 'active' : '' }} step-1-menu" data-for=".step-1">
            <h4 class="list-group-item-heading">Kuota Parameter
                <span class="pull-right"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></span>
            </h4>
        </a>
        <a href="{{ route('agents.index') }}" class="list-group-item {{ Request::segment(3) == 'agents' ? 'active' : '' }} step-1-menu" data-for=".step-1">
            <h4 class="list-group-item-heading">Agents
                <span class="pull-right"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></span>
            </h4>
        </a>
        <a href="{{ route('respondens.index') }}" class="list-group-item {{ Request::segment(3) == 'respondens' ? 'active' : '' }} step-1-menu" data-for=".step-1">
            <h4 class="list-group-item-heading">Responden
                <span class="pull-right"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></span>
            </h4>
        </a>
        <a href="{{ route('assignment.kuota') }}" class="list-group-item {{ Request::segment(3) == 'assignment' ? 'active' : '' }} step-1-menu" data-for=".step-1">
            <h4 class="list-group-item-heading">Assignment Process
                <span class="pull-right"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></span>
            </h4>
        </a>
        <a href="{{ route('perolehan.index') }}" class="list-group-item {{ Request::segment(3) == 'perolehan' ? 'active' : '' }} step-1-menu" data-for=".step-1">
            <h4 class="list-group-item-heading">Rubah Status Responden
                <span class="pull-right"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></span>
            </h4>
        </a>
        <a href="{{ route('perolehan.byTanggal') }}" class="list-group-item {{ Request::segment(3) == 'perolehanbyTanggal' ? 'active' : '' }} step-1-menu" data-for=".step-1">
            <h4 class="list-group-item-heading">Perolehan Responden By Tanggal
            </h4>
        </a>
        <a href="{{ route('perolehan.byKuota') }}" class="list-group-item {{ Request::segment(3) == 'perolehanbyKuota' ? 'active' : '' }} step-1-menu" data-for=".step-1">
            <h4 class="list-group-item-heading">Perolehan Responden By Kuota
            </h4>
        </a>
        <a href="{{ route('perolehan.byStatus') }}" class="list-group-item {{ Request::segment(3) == 'perolehanbyStatus' ? 'active' : '' }} step-1-menu" data-for=".step-1">
            <h4 class="list-group-item-heading">Perolehan Responden By Status
            </h4>
        </a>
        <a href="{{ route('perolehan.byAgent') }}" class="list-group-item {{ Request::segment(3) == 'perolehanbyAgent' ? 'active' : '' }} step-1-menu" data-for=".step-1">
            <h4 class="list-group-item-heading">Perolehan Responden By Agent
            </h4>
        </a>

        <a href="{{ route('listrekaman.index') }}" class="list-group-item {{ Request::segment(3) == 'listrekaman' ? 'active' : '' }} step-1-menu" data-for=".step-1">
            <h4 class="list-group-item-heading">Upload Rekaman
                <span class="pull-right"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></span>
            </h4>
        </a>
    </div>
</div>
