<p>List Rekaman responden {{ $responden->first_name }}</p>

<table class="table table-responsive table-hover table-striped">
  <thead>
    <th>Rekaman</th>
    <th>Tanggal</th>
    <th>Rekaman</th>
  </thead>
  <tbody>

    @foreach($listRekamans as $list)
      <tr>
        <td>{{ $list->nama_rekaman }}</td>
        <td>{{ $list->tgl_call }}</td>
        <td>
          <audio  controls>
            <source src="{!! Storage::url('/rekaman/'.$list->nama_rekaman) !!}" >
          </audio>
        </td>
      </tr>
    @endforeach

    
  </tbody>
</table>