@extends('layouts.app')


@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="panel-heading">List Project</div>

                <div class="panel-body">
                    
                    <div class="row">
                        
                        @foreach($projects as $project)
                          <div class="col-md-3">
                             <div class="panel panel-primary">
                                <div class="panel-heading">
                                   <div class="row">
                                      <div class="col-xs-4">
                                         <i class="fa fa-usd fa-5x"></i>
                                      </div>
                                      <div class="col-xs-8 text-right">
                                         <p class="announcement-text">{{ $project->name }}</p>
                                         <p class="announcement-text">{{ $project->target_responden }} responden</p>
                                      </div>
                                   </div>
                                </div>
                                <a href="{{ route('admin.dashboard.redirect',$project->id) }}">
                                   <div class="panel-footer announcement-bottom">
                                      <div class="row">
                                         <div class="col-xs-6">
                                            Enter
                                         </div>
                                         <div class="col-xs-6 text-right">
                                            <i class="fa fa-arrow-circle-right"></i>
                                         </div>
                                      </div>
                                   </div>
                                </a>
                             </div>
                          </div>
                        @endforeach


                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

