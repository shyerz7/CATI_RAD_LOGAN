@extends('layouts.app')


@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('vendors/yadcf/jquery.dataTables.yadcf.css') }}">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="{!! asset('css/toastr.min.css') !!}">
@endsection

@section('content')
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">

                {{-- Untuk Pesan Sukses --}}
                @if(Session::has('alert-success'))
                    <div class="alert alert-success">
                        {{ Session::get('alert-success') }}
                    </div>
                @endif

            </div>
        </div>

        <div class="row">

            @include('backend.partials.sidebar')

            <div class="col-md-9">

                <div class="panel with-nav-tabs panel-default">
                    <div class="panel-heading">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#bystatus" data-toggle="tab">Perolehan by status</a></li>

                        </ul>
                    </div>
                    <div class="panel-body">
                        <div class="tab-content">


                            <div class="tab-pane fade in active " id="bystatus">
                                <table id="perolehanByStatus" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                    <table class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                        <th>Status</th>
                                        <th>Jumlah Responden</th>
                                        </thead>
                                        <tbody>
                                        @foreach($statuses as $status)
                                            <tr>
                                                <td>
                                  <span class="{{ $status->label }}">
                                  {{ $status->name }}
                                  </span>
                                                </td>
                                                <td>
                                                    <?php

                                                    $project = \Session::get('session_project');

                                                    $count = App\Models\Responden::where('project_id',$project->id)
                                                            ->where('last_status_id',$status->id)->count();

                                                    ?>
                                                    <a href="{{ route('perolehan.perolehanRespByStatus',$status->id) }}" class="btn btn-primary btn-xs"> {{ $count or '-' }} <i class="fa fa-download" aria-hidden="true"></i>  </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection

@section('js')
    <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>

    {{-- <script src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
    <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script>
    <script src="//cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script> --}}
    <script src="{!! asset('js/toastr.min.js') !!}"></script>
    <script src="{{ asset('js/loader.js') }}"></script>
    <script>
        $(document).ready(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            // $('#perolehanByStatus').DataTable();
            // $('#perolehanByAgent').DataTable();
            $('#respondens').DataTable();

            $("#btnCari").button().click(function(e){
                e.preventDefault();

                var dateFrom  = $('#dateFrom').val();
                var dateTo    = $('#dateTo').val();
                var status_id = $('#status_id').val();

                $.ajax({
                    method: 'GET',
                    url: '{{ route('perolehan.perolehanRespByDate') }}',
                    data: {
                        dateFrom: dateFrom,
                        dateTo: dateTo,
                        status_id: status_id
                    },
                    success: function(data) {
                        $('#hasil_cari').html(data);
                    },error:function(result){
                        toastr.error('Failed to search Data Bro');
                    }
                });
            });



        });
    </script>
@endsection
