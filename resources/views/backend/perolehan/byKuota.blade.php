@extends('layouts.app')


@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('vendors/yadcf/jquery.dataTables.yadcf.css') }}">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="{!! asset('css/toastr.min.css') !!}">
@endsection

@section('content')
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">

                {{-- Untuk Pesan Sukses --}}
                @if(Session::has('alert-success'))
                    <div class="alert alert-success">
                        {{ Session::get('alert-success') }}
                    </div>
                @endif

            </div>
        </div>

        <div class="row">

            @include('backend.partials.sidebar')

            <div class="col-md-9">

                <div class="panel with-nav-tabs panel-default">
                    <div class="panel-heading">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#bykuota" data-toggle="tab">Perolehan by kuota</a></li>

                        </ul>
                    </div>
                    <div class="panel-body">
                        <div class="tab-content">
                            <p class="navbar-text navbar-left label label-success " style="color:white;">Hanya status SUKSES</p>
                            <div class="tab-pane fade in active " id="bykuota">
                                <table class="table table-striped table-bordered" cellspacing="0" width="100%" id="example">
                                    <thead>
                                    <th>Kuota</th>
                                    <th>Jumlah Kuota</th>
                                    <th>Jumlah Perolehan</th>
                                    <th></th>
                                    </thead>
                                    <tbody>
                                    @foreach($kuotas as $kuota)
                                        <tr>
                                            <td>{{ $kuota->nama_parameter }}</td>
                                            <td>{{ $kuota->kuota }}</td>
                                            <td>
                                                <?php
                                                $project = \Session::get('session_project');

                                                $jmlResp = \App\Models\Responden::where('project_id',$project->id)
                                                        ->where('kuota_parameter_id',$kuota->id)
                                                        ->where('last_status_id',1) // sukses
                                                        ->count();
                                                ?>
                                                <a href="{{ route('perolehan.perolehanRespByKuota',$kuota->id) }}" class="btn btn-primary btn-xs">{{  $jmlResp or '0' }} <i class="fa fa-download" aria-hidden="true"></i></a>

                                            </td>
                                            <td>{{ $kuota->updated_at }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection

@section('js')
    <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>

    {{-- <script src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
    <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script>
    <script src="//cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script> --}}
    <script src="{!! asset('js/toastr.min.js') !!}"></script>
    <script src="{{ asset('js/loader.js') }}"></script>
    <script>
        $(document).ready(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            // $('#perolehanByStatus').DataTable();
            // $('#perolehanByAgent').DataTable();
            $('#respondens').DataTable();

            $("#btnCari").button().click(function(e){
                e.preventDefault();

                var dateFrom  = $('#dateFrom').val();
                var dateTo    = $('#dateTo').val();
                var status_id = $('#status_id').val();

                $.ajax({
                    method: 'GET',
                    url: '{{ route('perolehan.perolehanRespByDate') }}',
                    data: {
                        dateFrom: dateFrom,
                        dateTo: dateTo,
                        status_id: status_id
                    },
                    success: function(data) {
                        $('#hasil_cari').html(data);
                    },error:function(result){
                        toastr.error('Failed to search Data Bro');
                    }
                });
            });



        });
    </script>
@endsection
