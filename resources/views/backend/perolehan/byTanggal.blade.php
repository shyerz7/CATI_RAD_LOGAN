@extends('layouts.app')


@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('vendors/yadcf/jquery.dataTables.yadcf.css') }}">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="{!! asset('css/toastr.min.css') !!}">
@endsection

@section('content')
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">

                {{-- Untuk Pesan Sukses --}}
                @if(Session::has('alert-success'))
                    <div class="alert alert-success">
                        {{ Session::get('alert-success') }}
                    </div>
                @endif

            </div>
        </div>

        <div class="row">

            @include('backend.partials.sidebar')

            <div class="col-md-9">

                <div class="panel with-nav-tabs panel-default">
                    <div class="panel-heading">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#bydate" data-toggle="tab">Perolehan by Tanggal</a></li>

                        </ul>
                    </div>
                    <div class="panel-body">
                        <div class="tab-content">

                            <div class="tab-pane fade in active" id="bydate">

                                <div class="alert alert-info" role="alert"> <strong>Perhatian !</strong> Fitur  ini berjalan di browser Chrome </div>

                                <div class="form-inline">

                                    <div class="form-group">
                                        <label for="email">From</label>
                                        <input type="date" name="dateFrom" id="dateFrom"  class="form-control"  />
                                    </div>

                                    <div class="form-group">
                                        <label for="email">To</label>
                                        <input type="date" name="dateTo" id="dateTo" class="form-control"  />
                                    </div>

                                    <div class="form-group">
                                        {{-- {!!  Form::select('status_id', $listStatus, null, array('class'=>'form-control','id'=>'status_id'))  !!} --}}
                                        <select class="form-control" id="status_id" name="status_id">
                                            <option value="1">Sukses Interview</option>
                                            <option value="2">Menolak</option>
                                            <option value="3">Nomor tidak terpasang</option>
                                            <option value="4">Reshcedule</option>
                                            <option value="5">Salah Sambung</option>
                                            <option value="6">Setengah Wawancara</option>
                                            <option value="7">Tidak ada Jawaban</option>
                                            <option value="8">Tidak ada nada</option>
                                            <option value="9">Tidak Aktif</option>
                                            <option value="10">Tidak Lolos Screening</option>
                                            <option value="11">Kuota Sudah Full</option>
                                            <option value="99">Fresh List</option>
                                            <option value="100">All Status</option>
                                        </select>
                                    </div>

                                    <input id = "btnCari" type="submit" class="btn btn-default" value="Cari"/>
                                </div>

                                <br>

                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Data yang dicari</h3>
                                    </div>
                                    <div class="panel-body">
                                        <div id="hasil_cari">

                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection

@section('js')
    <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>

    {{-- <script src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
    <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script>
    <script src="//cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script> --}}
    <script src="{!! asset('js/toastr.min.js') !!}"></script>
    <script src="{{ asset('js/loader.js') }}"></script>
    <script>
        $(document).ready(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            // $('#perolehanByStatus').DataTable();
            // $('#perolehanByAgent').DataTable();
            $('#respondens').DataTable();

            $("#btnCari").button().click(function(e){
                e.preventDefault();

                var dateFrom  = $('#dateFrom').val();
                var dateTo    = $('#dateTo').val();
                var status_id = $('#status_id').val();

                $.ajax({
                    method: 'GET',
                    url: '{{ route('perolehan.perolehanRespByDate') }}',
                    data: {
                        dateFrom: dateFrom,
                        dateTo: dateTo,
                        status_id: status_id
                    },
                    success: function(data) {
                        $('#hasil_cari').html(data);
                    },error:function(result){
                        toastr.error('Failed to search Data Bro');
                    }
                });
            });



        });
    </script>
@endsection
