<div class="col-md-5">
  <div class="panel panel-success">
    <div class="panel-heading">
       <div class="row">
          <div class="col-xs-4">
             <h1>{{ $respondens->count() }} </h1>
          </div>
          <div class="col-xs-8 text-right">
             <p class="announcement-text">{{ $project->name }}</p>
             <p class="announcement-text">{{ $status->name or 'All' }}</p>
          </div>
       </div>
    </div>
      
       <form class="form-horizontal" role="form" method="POST" action="{{ route('perolehan.perolehanRespByDateExport') }}">
        
       {{ csrf_field() }}

      <input type="hidden" name="dateFrom" id="dateFrom"  value="{!! $convDateFrom !!}" />
      <input type="hidden" name="dateTo" id="dateTo" value="{!! $convDateTo !!}"  />
      <input type="hidden" name="status_id" id="status_id" value="{!! $status_id !!}"  />

      <button id="exportHasilCari" name="exportHasilCari" class="btn btn-success"> <i class="fa fa-cogs" aria-hidden="true"></i> Export to Excel</button>

      </form>
 </div>
</div>

<script>
  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });


  
</script>