@extends('layouts.app')


@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="{{ asset('vendors/yadcf/jquery.dataTables.yadcf.css') }}">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="{!! asset('css/toastr.min.css') !!}">
@endsection

@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-md-12">

          {{-- Untuk Pesan Sukses --}}
          @if(Session::has('alert-success'))
              <div class="alert alert-success">
                    {{ Session::get('alert-success') }}
                </div>
          @endif

        </div>
      </div>

    <div class="row">

        @include('backend.partials.sidebar')

        <div class="col-md-9">

          <div class="panel with-nav-tabs panel-default">
              <div class="panel-heading">
                          <li class="active"><a href="#rubahStatus" data-toggle="tab">Rubah Status Agent</a></li>
                          
                      </ul>
              </div>
              <div class="panel-body">
                  <div class="tab-content">

                      <div class="tab-pane fade in active " id="rubahStatus">
                        
                         <table id="respondens" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Kuota</th>
                                    <th>RespID</th>
                                    <th>FirstName</th>
                                    <th>Status</th>
                                    <th>No Telpon</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            
                            <tbody>
                                
                                @foreach($respondens as $responden)
                                    <tr>  
                                        <td>{{ $responden->kuota->nama_parameter }}</td>
                                        <td>{{ $responden->resp_id }}</td>
                                        <td>{{ $responden->first_name }}</td>
                                        <td>
                                            <span class="{{ $responden->status->label }}">
                                                {{ $responden->status->name }}
                                            </span>
                                        </td>
                                        <td>{{ $responden->nomer_hp }}</td>
                                        
                                        <td>
                                            <div class="btn-group" role="group" aria-label="...">
                                              <a href="{{ route('assignment.viewRespDetail',[$responden->agent_id,$responden->kuota_parameter_id,$responden->id]) }}" class="btn btn-success">Rubah Status</a>
                                            </div>
                                        </td>
                                    </tr>

                                @endforeach
                                
                            </tbody>
                        </table>

                      </div>
                  </div>
              </div>
          </div>

        </div>
    </div>
</div>

@endsection

@section('js')
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>

{{-- <script src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script>
<script src="//cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script> --}}
<script src="{!! asset('js/toastr.min.js') !!}"></script>
<script src="{{ asset('js/loader.js') }}"></script>
<script>
  $(document).ready(function() {

    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });

    // $('#perolehanByStatus').DataTable();
    // $('#perolehanByAgent').DataTable();
    $('#respondens').DataTable();

    $("#btnCari").button().click(function(e){
        e.preventDefault();
        
        var dateFrom  = $('#dateFrom').val();
        var dateTo    = $('#dateTo').val();
        var status_id = $('#status_id').val();

        $.ajax({
          method: 'GET',
          url: '{{ route('perolehan.perolehanRespByDate') }}',
          data: {
              dateFrom: dateFrom,
              dateTo: dateTo,
              status_id: status_id
          },
          success: function(data) {
            $('#hasil_cari').html(data);
          },error:function(result){
            toastr.error('Failed to search Data Bro');
          }
        });
    });  

    

  });
</script>
@endsection
