@extends('layouts.app')


@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="{{ asset('css/sweetalert2.css') }}">
<style>
  .widget {
    margin: 0 0 25px 0;
    display: block;
    -webkit-border-radius: 2px;
    -moz-border-radius: 2px;
    border-radius: 2px;
}
.widget .widget-heading {
    padding: 7px 15px;
    -webkit-border-radius: 2px 2px 0 0;
    -moz-border-radius: 2px 2px 0 0;
    border-radius: 2px 2px 0 0;
    text-transform: uppercase;
    text-align: center;
    background: #38BDFF;
    color: white;
}
.widget .widget-body {
    padding: 10px 15px;
    font-size: 36px;
    font-weight: 300;
    background: #8FDEFF;
}
</style>
@endsection


@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-md-12">

          {{-- Untuk Pesan Sukses --}}
          @if(Session::has('alert-success'))
              <div class="alert alert-success">
                    {{ Session::get('alert-success') }}
                </div>
          @endif

          @if(Session::has('alert-gagal'))
              <div class="alert alert-danger">
                    {{ Session::get('alert-gagal') }}
                </div>
          @endif

        </div>
      </div>

    <div class="row">

        @include('backend.partials.sidebar')

        <div class="col-md-9">
            <div class="panel panel-default">

                <div class="panel-body">

                  <!-- Nav tabs, ini tombol tab di atas -->
                  <ul class="nav nav-tabs">
                  <!-- Untuk Semua Tab.. pastikan a href=”#nama_id” sama dengan nama id di “Tap Pane” dibawah-->
                    <li class="active"><a href="#datatable" data-toggle="tab">Datatable</a></li> <!-- Untuk Tab pertama berikan li class=”active” agar pertama kali halaman di load tab langsung active-->
                    <li><a href="#excel" data-toggle="tab">Input Responden Via Excel</a></li>
                  </ul>
                  <!-- Tab panes, ini content dari tab di atas -->
                  <div class="tab-content">

                     <div class="tab-pane active" id="datatable">
                        <br>
                        <a href="{{ route('kuotas.create')}}" class="btn btn-primary">Tambah Nama Parameter Kuota</a>
                         <br><br>
                         <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                              <thead>
                                  <tr>
                                      <th>Nama Parameter Kuota</th>
                                      <th>Kuota yang diset</th>
                                      <th>Action</th>
                                  </tr>
                              </thead>

                              <tbody>

                                  @foreach($kuotas as $kuota)
                                      <tr>
                                          <td>{{ $kuota->nama_parameter }}</td>
                                          <td>{{ $kuota->kuota }}</td>

                                          <td>
                                              <div class="btn-group" role="group" aria-label="...">
                                                <a href="{{ route('kuotas.edit',$kuota->id) }}" class="btn btn-success">Edit</a>
                                                <a href="#" data-target="{{ route('kuotas.destroy', [$kuota->id]) }}" class="btn btn-danger confirmation" onclick="hapusData(this)">Hapus</a>
                                              </div>
                                          </td>
                                      </tr>
                                  @endforeach

                              </tbody>
                         </table>

                     </div>

                     <div class="tab-pane" id="excel">

                        <div class="col-md-6">
                          <br>
                          <div class="alert alert-warning" role="alert"> <strong>Warning!</strong>
                            <ul>
                              <li>untuk ID kuota paramter adalah autoincrement</li>
                            </ul>
                          </div>

                        </div>

                        <div class="col-md-6">
                          <a href="{{ route('kuotas.generateexcel') }}" class="btn btn-success"><i class="fa fa-download" aria-hidden="true"></i> Download Template Excel</a>
                          <br>
                          <form method="POST" action="{{ route('admin.kuotas.importExcel') }}" accept-charset="UTF-8" class="" enctype="multipart/form-data">

                              {{ csrf_field() }}

                              <hr>

                              <div class="form-group">
                                  <label class="control-label " for="nama">
                                      Import Responden dari Excel
                                  </label>
                                  <input class="form-control filestyle" data-icon="false" id="excel" name="excel" type="file" required="">
                              </div>

                              <div class="form-group">
                                  <input name="save" type="submit" value="Save" class="btn btn-primary" onclick="waitingDialog.show();setTimeout(function () {waitingDialog.hide();}, 3000);">
                              </div>

                          </form>
                        </div>

                     </div>

                  </div>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>
<script src="{{ asset('js/sweetalert2.min.js') }}"></script>
<script src="{{ asset('js/confirm.js') }}"></script>
<script src="{{ asset('js/loader.js') }}"></script>
<script>
  $(document).ready(function() {
    $('#example').DataTable();
} );
</script>
@endsection
