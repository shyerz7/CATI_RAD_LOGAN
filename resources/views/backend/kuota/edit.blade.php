@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">

        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Create Parameter Kuota</div>
                <div class="panel-body">

                   {!!  Form::model($kuota, array('method' => 'put', 'route' => array('kuotas.update', $kuota->id), 'files'=> false,'class'=>'form-horizontal')) !!}

                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="nama_parameter" class="col-md-4 control-label">Nama Paramter Kuota</label>

                            <div class="col-md-6">
                                <input id="nama_parameter" type="text" class="form-control" name="nama_parameter" value="{{ $kuota->nama_parameter or '' }}" required autofocus>

                                @if ($errors->has('nama_parameter'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('nama_parameter') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('kuota') ? ' has-error' : '' }}">
                            <label for="kuota" class="col-md-4 control-label">Kuota</label>

                            <div class="col-md-6">
                                <input id="kuota" type="text" class="form-control" name="kuota" value="{{ $kuota->kuota or '' }}" required autofocus>

                                @if ($errors->has('kuota'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('kuota') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        


                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
