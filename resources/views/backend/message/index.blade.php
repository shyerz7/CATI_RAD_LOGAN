@extends('layouts.app')


@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="{{ asset('css/sweetalert2.css') }}">
@endsection


@section('content')
<div class="container-fluid">
    
    <div class="row">
        <div class="col-md-12">

          {{-- Untuk Pesan Sukses --}}
          @if(Session::has('alert-success'))
              <div class="alert alert-success">
                    {{ Session::get('alert-success') }}
                </div>
          @endif

        </div>
      </div>

    <div class="row">
        
        @include('backend.partials.sidebar')

        <div class="col-md-9">
            <div class="panel panel-default">

                <div class="panel-body">
                    
                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Tgl Pesan</th>
                                <th>Agent Code</th>
                                <th>Agent Nama</th>
                                <th>Pesan</th>
                                <th>Action</th>
                                <th>Sudah ditangani ?</th>
                            </tr>
                        </thead>
                        
                        <tbody>
                            
                            @foreach($messages as $message)
                                <tr>
                                    <td>
                                        <?php
                                            $conv = Carbon\Carbon::parse($message->created_at);
                                            $tgl  = $conv->toDateString();
                                        ?>
                                        {{ $tgl }}
                                    </td>
                                    <td>{{ $message->agent->code }}</td>
                                    <td>{{ $message->nama_agent }}</td>
                                    <td>{{ $message->msg }}</td>
                                    <td>
                                        @if($message->is_read == true)
                                            -
                                        @else
                                            <a href="{{ route('pesan.edit',$message->id) }}" class="btn btn-success">Solved</a>
                                        @endif
                                        
                                    </td>
                                    <td>
                                        @if($message->is_read == true)
                                            <span class="label label-success">Sudah</span>
                                        @else
                                            <span class="label label-warning">Belum</span>
                                        @endif
                                    </td>
                                    
                                </tr>
                            @endforeach
                            
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>
<script src="{{ asset('js/sweetalert2.min.js') }}"></script>
<script src="{{ asset('js/confirm.js') }}"></script>
<script>
  $(document).ready(function() {
    $('#example').DataTable();
} );
</script>
@endsection

