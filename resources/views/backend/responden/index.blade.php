@extends('layouts.app')


@section('css')
<link rel="stylesheet" href="{{ asset('css/dataTables.bootstrap.css') }}">
<!--<link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">-->
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css">

<link rel="stylesheet" href="{{ asset('css/sweetalert2.css') }}">
@endsection


@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-md-12">

          {{-- Untuk Pesan Sukses --}}
          @if(Session::has('alert-success'))
              <div class="alert alert-success">
                    {{ Session::get('alert-success') }}
                </div>
          @endif

          {{-- Untuk Pesan Error --}}
          @if(Session::has('flash_notification'))
              <div class="alert alert-danger">
                    {{ Session::get('flash_notification') }}
                </div>
          @endif

        </div>
      </div>

    <div class="row">

        @include('backend.partials.sidebar')

        <div class="col-md-9">

            <div class="panel panel-default">
                <div class="panel-body">

                    <!-- Nav tabs, ini tombol tab di atas -->
                    <ul class="nav nav-tabs">
                    <!-- Untuk Semua Tab.. pastikan a href=”#nama_id” sama dengan nama id di “Tap Pane” dibawah-->
                      <li class="active"><a href="#datatable" data-toggle="tab">Datatable</a></li> <!-- Untuk Tab pertama berikan li class=”active” agar pertama kali halaman di load tab langsung active-->
                      <li><a href="#excel" data-toggle="tab">Input Responden Via Excel</a></li>
                    </ul>
                    <!-- Tab panes, ini content dari tab di atas -->
                    <div class="tab-content">

                      <div class="tab-pane active" id="datatable">
                        <br>
                        <a href="{{ route('respondens.create') }}" class="btn btn-primary">Tambah Responden</a>
                        <a href="{{ route('exportRespondens') }}" class="btn btn-success"><i class="fa fa-download" aria-hidden="true"></i> Export Respondens</a>
                        <br><br>
                        <table id="respondens-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                          <thead>
                              <tr>
                                  <th>RespID</th>
                                  <th>First Name</th>
                                  <th>Last Name</th>
                                  <th>Action</th>
                              </tr>
                          </thead>
                          <tfoot>
                            <tr>
                                  <th>RespID</th>
                                  <th>First Name</th>
                                  <th>Last Name</th>
                                  <th>Action</th>
                              </tr>
                          </tfoot>

                        </table>
                      </div><!-- Untuk Tab pertama berikan div class=”active” agar pertama kali halaman di load content langsung active-->

                      <div class="tab-pane" id="excel">
                      <br>

                        <div class="col-md-6">

                          <div class="alert alert-warning" role="alert"> <strong>Warning!</strong>
                            <ul>
                              <li>Pengisian Responden ID,First Name, Nomer HP 1 , Link Survey adalah wajib</li>
                              <li>Mode proses upload ini adalah Create-Update, asalkan responden id tidak dirubah</li>
                              <li>Responden yang diupload otomatis berstatus Fresh List</li>
                              <li>Untuk kuota parameter ID, isikan dengan <strong>KODE dibawah ini</strong> </li>
                            </ul>
                          </div>

                          <table class="table table-striped" id="inputResponden">
                            <thead>
                              <tr>
                                <th>ID</th>
                                <th>Nama Kuota</th>
                                <th>Jumlah Kuota</th>
                              </tr>
                            </thead>
                            <tbody>
                              @foreach($kuotas as $kuota)
                                <tr>
                                  <td>
                                    <span class="label label-success">
                                      {{ $kuota->id }}
                                    </span>
                                  </td>
                                  <td>{{ $kuota->nama_parameter }}</td>
                                  <td>{{ $kuota->kuota }}</td>
                                </tr>
                              @endforeach
                            </tbody>
                          </table>

                        </div>

                        <div class="col-md-6">
                          <br>
                          <a href="{{ route('respondens.generateexcel') }}" class="btn btn-success"><i class="fa fa-download" aria-hidden="true"></i> Download Template Excel</a>
                          <br>

                          <form method="POST" action="{{ route('admin.respondens.importExcel') }}" accept-charset="UTF-8" class="" enctype="multipart/form-data">

                              {{ csrf_field() }}

                              <hr>

                              <div class="form-group">
                                  <label class="control-label " for="nama">
                                      Import Responden dari Excel
                                  </label>
                                  <input class="form-control filestyle" data-icon="false" id="excel" name="excel" type="file" required="">
                              </div>

                              <div class="form-group">
                                  <input name="save" type="submit" value="Save" class="btn btn-primary" onclick="waitingDialog.show();setTimeout(function () {waitingDialog.hide();}, 3000);">
                              </div>

                          </form>


                      </div>
                      </div>

                    </div>



                </div>


            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/datatables.bootstrap.js') }}"></script>
<!--<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>-->
<script src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script>
<script src="//cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>

<script src="{{ asset('js/sweetalert2.min.js') }}"></script>
<script src="{{ asset('js/confirm.js') }}"></script>
<script src="{{ asset('js/loader.js') }}"></script>
<script>
  $(document).ready(function() {
    $('#example').DataTable();

    $('#inputResponden').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'copyHtml5',
                exportOptions: {
                 columns: ':contains("Office")'
                }
            },
            'excelHtml5',
            'csvHtml5',
        ]
    } );

    $('#respondens-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{{ route('api.respondens')  }}',
        columns: [
            {data: 'resp_id', name: 'resp_id'},
            {data: 'first_name', name: 'first_name'},
            {data: 'last_name', name: 'last_name'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
        initComplete: function () {
            this.api().columns().every(function () {
                var column = this;
                var input = document.createElement("input");
                $(input).appendTo($(column.footer()).empty())
                .on('change', function () {
                    column.search($(this).val(), false, false, true).draw();
                });
            });
        }
    });

  } );
</script>
@endsection
