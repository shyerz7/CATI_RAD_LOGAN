@extends('layouts.app')


@section('content')
<div class="container-fluid">
    <div class="row">
        
        @include('backend.partials.sidebar')

        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">
                  Create Responden
                </div>

                <div class="panel-body">
                    
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('respondens.store') }}">
                    
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('resp_id') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Kuota Parameter ID</label>

                            <div class="col-md-6">
                                {!!  Form::select('kuota_parameter_id', $kuotas, old('kuota_parameter_id'), array('class'=>'form-control fisik_id'))  !!}
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('resp_id') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Kode Responden</label>

                            <div class="col-md-6">
                                <input id="resp_id" type="text" class="form-control" name="resp_id" value="{{ old('resp_id') }}" required autofocus>

                                @if ($errors->has('resp_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('resp_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('resp_id') ? ' has-error' : '' }}">
                            <label for="first_name" class="col-md-4 control-label">First Name</label>

                            <div class="col-md-6">
                                <input id="first_name" type="text" class="form-control" name="first_name" value="{{ old('first_name') }}" required autofocus>

                                @if ($errors->has('first_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('resp_id') ? ' has-error' : '' }}">
                            <label for="last_name" class="col-md-4 control-label">Last Name</label>

                            <div class="col-md-6">
                                <input id="last_name" type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" required autofocus>

                                @if ($errors->has('last_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('resp_id') ? ' has-error' : '' }}">
                            <label for="last_name" class="col-md-4 control-label">Email</label>

                            <div class="col-md-6">
                                <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}" >

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('link_survey') ? ' has-error' : '' }}">
                            <label for="link_survey" class="col-md-4 control-label">Link Survey</label>

                            <div class="col-md-6">
                                <input id="link_survey" type="text" class="form-control" name="link_survey" value="{{ old('link_survey') }}" required autofocus>

                                @if ($errors->has('link_survey'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('link_survey') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('link_survey') ? ' has-error' : '' }}">
                            <label for="link_expired" class="col-md-4 control-label">Link Expired</label>

                            <div class="col-md-6">
                                <input id="link_survey" type="date" class="form-control" name="link_expired" value="{{ old('link_expired') }}" >

                                @if ($errors->has('link_expired'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('link_expired') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                       <div class="form-group ">
                            <label for="nomor_hp" class="col-md-4 control-label">Nomor HP</label>
                            
                            <div class="col-md-6">
                                <input class="form-control" id="teks" name="teks[]" type="text" required="required" />
                                <div class="clone"></div>
                                <br>
                                <a class="btn btn-primary" id="add_text" href="javascript:void(0);">
                                    <i class="fa fa-plus"></i>
                                    Tambah Nomer HP
                                </a>
                            </div>

                            
                        </div>      

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Save
                                </button>

                            </div>
                        </div>
                    </form>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('js')
  
    <script src="{{ asset('js/clone-text.js') }}"></script>
  
@endsection



