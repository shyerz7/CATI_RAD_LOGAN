@extends('layouts.app')


@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="{{ asset('css/sweetalert2.css') }}">
@endsection


@section('content')
<div class="container-fluid">
    
    <div class="row">
        <div class="col-md-12">

          {{-- Untuk Pesan Sukses --}}
          @if(Session::has('alert-success'))
              <div class="alert alert-success">
                    {{ Session::get('alert-success') }}
                </div>
          @endif

        </div>
      </div>

    <div class="row">
        
        @include('backend.partials.sidebar')

        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">
                  List Agents
                  <a href="{{ route('agents.create') }}" class="btn btn-primary">Tambah Agent</a>
                </div>

                <div class="panel-body">
                    
                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Nama</th>
                                <th>Kode</th>
                                <th>Email</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        
                        <tbody>
                            
                            @foreach($agents as $agent)
                                <tr>
                                    <td>{{ $agent->name }}</td>
                                    <td>{{ $agent->code }}</td>
                                    <td>{{ $agent['user']['email']  }}</td>
                                    <td>
                                        <div class="btn-group" role="group" aria-label="...">
                                          <a href="{{ route('agents.edit',$agent->id) }}" class="btn btn-success">Edit</a>
                                          <a href="#" data-target="{{ route('agents.destroy', [$agent->id]) }}" class="btn btn-danger confirmation" onclick="hapusData(this)">Hapus</a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>
<script src="{{ asset('js/sweetalert2.min.js') }}"></script>
<script src="{{ asset('js/confirm.js') }}"></script>
<script>
  $(document).ready(function() {
    $('#example').DataTable();
} );
</script>
@endsection

