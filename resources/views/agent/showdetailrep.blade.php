@extends('layouts.app')

@section('css')
<link rel="stylesheet" href="{!! asset('css/bootstrap-datetimepicker.css') !!}">
<link rel="stylesheet" href="{!! asset('css/toastr.min.css') !!}">
<link rel="stylesheet" href="{{ asset('css/sweetalert2.css') }}">
@endsection

@section('content')

<div class="container-fluid">
    <div class="row">

        <div class="col-md-12">
             <div class="container-fluid">
                <div class="row">

                  <div class="col-md-4" >
                        <div class="panel panel-success">
                            <div class="panel-body">
                                <div class="form-horizontal">
                                    <fieldset>
                                  <form role="form" method="POST" action="{!! route('updateStatRespOnDetailPage',$responden->id) !!}">
                                  {!! csrf_field() !!}
                                    <!-- Form Name -->
                                    <legend>Screening Responden</legend>

                                    <!-- Select Basic -->
                                    <div class="form-group">
                                      <label class="col-md-4 control-label" for="selectbasic">Nomor HP</label>
                                      <div class="col-md-6">
                                        <select multiple id="nomer_hp" name="nomer_hp" class="form-control" onkeydown="keydown(event)" onchange="changeClipboardValue(this)">
                                          @foreach($list_nomor_hp as $nomer_hp)
                                             {{-- <option value="{!! $nomer_hp !!}">{!!  substr_replace($nomer_hp, 'xxxxx', 6, 6)  !!}</option>    --}}
                                             <option value="{!! $nomer_hp !!}">{!!  $nomer_hp  !!}</option>
                                          @endforeach
                                        </select>
                                      </div>
                                      <label class="col-md-4 control-label" for="clipboard">Copy Me</label>
                                      <div class="col-md-6">
                                        <input type="text" id="clipboard" onkeyup="keyup(event)" class="form-control" />
                                      </div>
                                    </div>

                                    <!-- Button -->
                                    {{-- <div class="form-group">
                                      <label class="col-md-4 control-label" for="singlebutton">Single Button</label>
                                      <div class="col-md-6">
                                        <a id="call" href="#" data-id='{!! $responden->id !!}'  class="btn btn-primary"><i class="fa fa-phone-square" aria-hidden="true"></i> Call</a>
                                        <a id="hangup" name="hangup" href="#" data-id='{!! $responden->id !!}'  class="btn btn-danger"><i class="fa fa-phone-square" aria-hidden="true"></i> Hangup</a>
                                      </div>
                                    </div> --}}

                                    <!-- Proses Survey -->
                                    <div class="form-group">
                                      <label class="col-md-4 control-label" for="singlebutton"></label>
                                      <div class="col-md-6">
                                        <a id="btnOnSurvey" href="#"  class="btn btn-success"><i class="fa fa-sign-in" aria-hidden="true"></i> Lanjut Survey</a>
                                      </div>
                                    </div>

                                    <hr>

                                    <div id="updatestatus">

                                      <!-- Select Basic -->
                                      <div class="form-group">
                                        <label class="col-md-4 control-label" for="status_id">Update Status Responden</label>
                                        <div class="col-md-6">
                                          {!! Form::select('status_id',$list_status,null, ['class' => 'form-control status_id']) !!}
                                        </div>
                                      </div>

                                      <!-- Textarea -->
                                      <div class="form-group div-note  ">
                                        <label class="col-md-4 control-label" for="textarea">Catatan</label>
                                        <div class="col-md-4">
                                          <textarea class="form-control" id="note" name="note"></textarea>
                                        </div>
                                      </div>

                                      <!-- Tanggal Reschedule -->
                                      <div class="form-group div-reschedule">
                                          <label class="col-md-4 control-label" for="textarea">Tanggal Reschedule</label>
                                          <div class="col-md-6">
                                              <div class='input-group date' id='datetimepicker2'>
                                                  <input type='text' id="datetimereschedule" name="datetimereschedule" class="form-control" />
                                                  <span class="input-group-addon">
                                                      <span class="glyphicon glyphicon-calendar"></span>
                                                  </span>
                                              </div>
                                          </div>
                                      </div>

                                      <div class="form-group">
                                          <label class="col-md-4 control-label" for="textarea">Nama Agent</label>
                                          <div class="col-md-6">
                                            <input type='text' id="agent_name" name="agent_name" class="form-control" required="require" />
                                          </div>
                                      </div>

                                      <!-- Proses Survey -->
                                      <div class="form-group">
                                        <label class="col-md-4 control-label" for="singlebutton"></label>
                                        <div class="col-md-6">
                                          <button id="singlebutton" name="singlebutton" class="btn btn-success"> <i class="fa fa-cogs" aria-hidden="true"></i> Update Status Responden</button>
                                        </div>
                                      </div>
                                    </div>




                                    </form>

                                    </fieldset>
                                    </div>
                            </div>
                        </div>
                  </div>

                  <div class="col-md-8" id="onsurvey" >
                      <div class="panel panel-primary">
                         <div class="panel-heading">
                            <h3 class="panel-title">{!! $responden->first_name !!} {!! $responden->last_name !!}</h3>
                         </div>
                         <div class="panel-body">

                            <div class="row">
                               <div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src="http://placehold.it/300x300" class="img-circle img-responsive"> </div>
                               <div class=" col-md-9 col-lg-9 ">
                                  <table class="table table-user-information">
                                     <tbody>
                                        <tr>
                                           <td>First Name:</td>
                                           <td><strong>{!! $responden->first_name !!}</strong></td>
                                        </tr>
                                        <tr>
                                           <td>Last Name</td>
                                           <td><strong>{!! $responden->last_name !!}</strong></td>
                                        </tr>
                                        <tr>
                                            <td>ID</td>
                                            <td><strong>{!! $responden->id !!}</strong></td>
                                        </tr>
                                        <tr>
                                           <td>Email</td>
                                           <td><strong>{!! $responden->email !!}</strong></td>
                                        </tr>
                                        <tr>
                                        <tr>
                                           <td>Call Attempt</td>
                                           <td><strong>{!! $responden->call_attempt or '-' !!}</strong></td>
                                        </tr>
                                        <tr>
                                           <td>Status Terakhir</td>
                                            <td><strong>{!! $responden->status->name or '-' !!}</strong></td>
                                        </tr>
                                        </tr>
                                     </tbody>
                                  </table>
                               </div>
                            </div>

                            <div class="row">
                              <fieldset style="padding-left: 20px;">Log Responden</fieldset>
                              <div class="col-md-12">
                                <table class="table table-responsive table-hover table-striped">
                                   <thead>
                                     <tr>
                                       <td>Status</td>
                                       <td>Nomor HP</td>
                                       <td>Note</td>
                                       <td>Tgl Reschedule</td>
                                       <td>Agent</td>
                                     </tr>
                                   </thead>
                                   <tbody>

                                      @foreach($log_respondens as $log)
                                         <tr>
                                           <td>
                                              <span class="{!! $log->status->label !!}">
                                                {!! $log->status->name !!}
                                              </span>
                                           </td>
                                           {{-- <td>{!! substr_replace($log->nomor_hp, 'xxxxx', 6, 6) !!}</td> --}}
                                           <td>{!! $log->nomor_hp !!}</td>
                                           <td>{!! $log->note !!}</td>
                                           <td>
                                             {!! $log->tgl_reschedule ? date("d/m/y, H:i:s", strtotime($log->tgl_reschedule)):'-' !!}
                                           </td>
                                           <td>{!! $log->agent_name or '-' !!}</td>
                                         </tr>
                                      @endforeach


                                   </tbody>
                                </table>
                              </div>
                            </div>

                         </div>
                      </div>
                   </div>

                </div>
             </div>
        </div>

    </div>
</div>

@endsection


@section('js')
<script src="{!! asset('js/moment-with-locales.js') !!}"></script>
<script src="{!! asset('js/bootstrap-datetimepicker.js') !!}"></script>
<script src="{!! asset('js/toastr.min.js') !!}"></script>
<script src="{{ asset('js/sweetalert2.min.js') }}"></script>
<script>
  $( document ).ready(function() {

    var i = 1;

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(".div-note").hide();
    $(".div-reschedule").hide();
    // $("#updatestatus").hide();

    /**
     * cek kuota full realtime
     */

    // setInterval(function()
    // {
    //     $.ajax({
    //       type:"get",
    //       url:"{{ route('ajaxCheckKuota',$responden->id) }}",
    //       success:function(data)
    //       {
    //         if(data == 'ok') {
    //           if(i == 1) {
    //             swal("Kuota Responden ini sudah full");
    //           }
    //         }
    //         i++;

    //       }
    //     });
    // }, 2000);
    //time in milliseconds


    $('.status_id').change(function() {
        if($('.status_id option:selected').val() == 4) {
            $(".div-note").show();
            $(".div-reschedule").show();
        }
        else {
            $("#note").val("");
            $("#datetimereschedule").val("");
            $(".div-note").hide();
            $(".div-reschedule").hide();
        }

    });

    $('#datetimepicker2').datetimepicker({
        locale: 'id',
        format:'YYYY-MM-DD hh:mm:00'
    });

    $("#call").click(function(e){
          e.preventDefault();

          var resp_id = $(this).attr('data-id');

          $.ajax({type: "POST",
              url: "{!! route('api.updateLockRespActive',$responden->id) !!}",
              success:function(result){
                toastr.success('Lock Responden');
                $("#updatestatus").show();
              },
              error:function(result)
              {
              toastr.error('Failed Lock Responden');
             }
          });
    });

    $("#hangup").click(function(e){
          e.preventDefault();

          var resp_id = $(this).attr('data-id');

          $.ajax({type: "POST",
              url: "{!! route('api.updateLockRespInActive',$responden->id) !!}",
              success:function(result){
                toastr.success('Unlock Responden');
                $("#updatestatus").show();
              },
              error:function(result)
              {
              toastr.error('Failed to Unlock Responden');
             }
          });
    });

    $('#btnOnSurvey').on('click', function(e){

      e.preventDefault();

      $.ajax({
        type: "POST",
        url: '{!! route('onsurvey.process',$responden->id) !!}',
        success: function(data) {
          $('#onsurvey').html(data);
        },error:function(result){
          toastr.error('Failed to Unlock Responden');
        }

       });

    });

  });
  /* copy paste select */
  window.holdingCTRL = false;

function changeClipboardValue(selectBox) {
    var clipboard = document.getElementById("clipboard");
    var text = "";
    for (i = 0; i < selectBox.length; i++) {
        if(selectBox.options[i].selected) text += selectBox.options[i].value;
    }
    clipboard.value = text;

    if(window.holdingCTRL) clipboard.select();
}

function keydown(e) {
    if(e.keyCode === 17) {
        var clipboard = document.getElementById("clipboard");
        clipboard.select();
        window.holdingCTRL = true;
    }
}

function keyup(e) {
    if(e.keyCode === 17) {
        var selectBox = document.getElementById("nomer_hp");
        selectBox.focus();
        window.holdingCTRL = false;
    }
}
/* end copy paste select*/
</script>

@endsection
