@extends('layouts.app')

@section('css')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">    
<link rel="stylesheet" href="{{ asset('css/bootstrap-datetimepicker.css') }}">    
<link rel="stylesheet" href="{{ asset('css/toastr.min.css') }}">  
@endsection

@section('content')

<!-- Main jumbotron for a primary marketing message or call to action -->
    {{-- <div class="alert alert-default">
      <div class="container">
        <div class="top-area scrolling-tabs-container inner-page-scroll-tabs">
          <h1>Survey Responden {{ $responden->first_name }} {{ $responden->last_name }} Sedang Berjalan</h1>
          <div class="pull-right">
        </div>
      </div>
    </div> --}}

    <div class="container-fluid">

      <!-- Example row of columns -->
      <div class="row">
        <div class="col-md-3">
          <p class="lead"><strong>{{ $responden->first_name }} {{ $responden->last_name }} </strong></p>
           
           <div class="row">
             
             <div class="col-md-12 form-horizontal">
               <form role="form" method="POST" action="{{ route('updateStatRespOnDetailPage',$responden->id) }}">
              
              {{ csrf_field() }}
              
              <div class="form-group">
                <label class="col-md-4 control-label" for="selectbasic">Nomor HP yang di call</label>
                <div class="col-md-6">
                  <input type="text" name="nomer_hp" id="nomer_hp" class="form-control" value="09100389300" readonly="">
                </div>
              </div>

              <!-- Select Basic -->
              <div class="form-group">
                <label class="col-md-4 control-label" for="status_id">Update Status </label>
                <div class="col-md-6">
                  {!! Form::select('status_id',$list_status,null, ['class' => 'form-control status_id']) !!}
                </div>
              </div>

              <!-- Textarea -->
              <div class="form-group div-note  ">
                <label class="col-md-4 control-label" for="textarea">Catatan</label>
                <div class="col-md-4">                     
                  <textarea class="form-control" id="note" name="note"></textarea>
                </div>
              </div> 

              <!-- Tanggal Reschedule -->
              <div class="form-group div-reschedule">
                  <label class="col-md-4 control-label" for="textarea">Tanggal Reschedule</label>
                  <div class="col-md-6">   
                      <div class='input-group date' id='datetimepicker2'>
                          <input type='text' id="datetimereschedule" name="datetimereschedule" class="form-control" />
                          <span class="input-group-addon">
                              <span class="glyphicon glyphicon-calendar"></span>
                          </span>
                      </div>
                  </div>
              </div>

              <!-- Proses Survey -->
              <div class="form-group">
                <label class="col-md-4 control-label" for="singlebutton"></label>  
                <div class="col-md-6">
                  <button id="singlebutton" name="singlebutton" class="btn btn-success"> <i class="fa fa-cogs" aria-hidden="true"></i> Proses</button>
                </div>
              </div>

            </form>
             </div>

           </div>

        </div>

          <div class="col-md-9">
            <div class="embed-responsive embed-responsive-16by9">
              <iframe class="embed-responsive-item" src="{{ $responden->link_survey }}"></iframe>
            </div>
          </div>
      </div>

@endsection


@section('js')
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>  
<script src="{{ asset('js/moment-with-locales.js') }}"></script>  
<script src="{{ asset('js/bootstrap-datetimepicker.js') }}"></script>  
<script src="{{ asset('js/toastr.min.js') }}"></script>  
  <script>
  $( document ).ready(function() {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(".div-note").hide();
    $(".div-reschedule").hide();

    $('.status_id').change(function() {
        if($('.status_id option:selected').val() == 4) {
            $(".div-note").show();
            $(".div-reschedule").show();
        }
        else {
            $("#note").val("");
            $("#datetimereschedule").val("");
            $(".div-note").hide();
            $(".div-reschedule").hide();
        }
        
    });

    $('#datetimepicker2').datetimepicker({
        locale: 'id',
        format:'YYYY-MM-DD hh:mm:00'
    });

    $("#call").click(function(e){
          e.preventDefault();

          var resp_id = $(this).attr('data-id');

          $.ajax({type: "POST",
              url: "{{ route('api.updateLockRespActive',$responden->id) }}",
              success:function(result){
                toastr.success('Lock Responden');
              },
              error:function(result)
              {
              toastr.error('Failed Lock Responden');
             }
          });
    });

    $("#hangup").click(function(e){
          e.preventDefault();

          var resp_id = $(this).attr('data-id');

          $.ajax({type: "POST",
              url: "{{ route('api.updateLockRespInActive',$responden->id) }}",
              success:function(result){
                toastr.success('Unlock Responden');
              },
              error:function(result)
              {
              toastr.error('Failed to Unlock Responden');
             }
          });
    });

  });
</script>
@endsection



