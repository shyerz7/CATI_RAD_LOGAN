@extends('layouts.app')


@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="{{ asset('css/sweetalert2.css') }}">
@endsection


@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-md-12">

          {{-- Untuk Pesan Sukses --}}
          @if(Session::has('alert-success'))
              <div class="alert alert-success">
                    {{ Session::get('alert-success') }}
                </div>
          @endif

        </div>
      </div>

    <div class="row">

        {{-- @include('backend.partials.sidebar') --}}

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                  List Responden yang harus di call
                  <span class="pull-right">
                    <button class="btn btn-primary" data-toggle="modal" data-target="#myModal" >Sampaikan masalah ke spv</button>
                  </span>

                </div>

                <div class="panel-body">

                  Jumlah Perolehan Responden: <a href="{{ route('enterproject.perolehanRespByAgent',$agent->id) }}" class="btn btn-primary btn-xs"> {{ $perolehanRespondenSukses->count() }} <i class="fa fa-download" aria-hidden="true"></i>  </a>
                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                          <thead>
                              <tr>
                                  <th>RespID</th>
                                  <th>First Name</th>
                                  <th>Kuota</th>
                                  <th>Call Attempt</th>
                                  <th>Status Terakhir</th>
                                  <th>Call Terakhir</th>
                                  <th>Action</th>
                              </tr>
                          </thead>

                          <tbody>

                              @foreach($respondens as $responden)

                                <?php

                                  $last_call_log = App\Models\LogResponden::where('responden_id',$responden->id)
                                                                          ->orderBy('id','desc')
                                                                          ->first();

                                  $format = Carbon\Carbon::parse($last_call_log['created_at']);




                                ?>

                                <!-- if untuk mengatur status label merah  -->
                                {{-- @if(!$format->isSameDay($datenow) || is_null($last_call_log) || ($responden->last_status_id == 4)) --}}

                                  {{-- @if($responden->call_attempt <= 3) --}}

                                    <tr>
                                      <td>{{ $responden->resp_id }}</td>
                                      <td>{{ $responden->first_name }}</td>
                                      <td>{{ $responden->kuota->nama_parameter }}</td>
                                      <td>{{ $responden->call_attempt or '-' }}</td>
                                      <td>

                                        <span class="{{ $responden->status->label}}">
                                          {{ $responden->status->name or '-' }}
                                        </span>


                                      </td>
                                      <td>
                                        <?php

                                          $last_call = App\Models\LogResponden::where('responden_id',$responden->id)
                                                                                ->orderBy('id','desc')
                                                                                ->first();

                                        ?>

                                        {{ $last_call ? date('d F Y g:i A', strtotime($last_call->created_at)) : '-' }}
                                      </td>
                                      <td>

                                        <?php
                                          //skrip untuk kuota
                                          $kuota  =App\Models\KuotaParameter::findOrFail($responden->kuota_parameter_id);

                                          $perolehanResp = App\Models\Responden::where('kuota_parameter_id',$kuota->id)
                                                                                 ->where('last_status_id',1)
                                                                                 ->count();

                                        ?>

                                        @if($perolehanResp >= $kuota->kuota)
                                        <span class="label label-danger">kuota sudah full</span>
                                        @else
                                          <div class="btn-group" role="group" aria-label="...">
                                            <a href="{{ route('showdetailrep',$responden->id) }}" class="btn btn-success">Survey Responden Ini</a>
                                          </div>
                                        @endif

                                        

                                      </td>
                                    </tr>

                                  {{-- @endif --}}

                                {{-- @endif --}}

                              @endforeach


                          </tbody>
                        </table>

                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Ada Kendala ? Sampaikan ke Supervisor</h4>
      </div>
      <div class="modal-body">

       <form class="form-horizontal" role="form" method="POST" action="{{ route('messages',$agent->id) }}">
          {{ csrf_field() }}
          <fieldset>

          <!-- Text input-->
          <div class="form-group">
            <label class="col-md-4 control-label" for="textinput">Agent Code</label>  
            <div class="col-md-8">
            <input id="textinput" name="textinput" type="text" value="{{ $agent->code }}"  class="form-control input-md" readonly="readonly">
            </div>
          </div>

          <!-- Text input-->
          <div class="form-group">
            <label class="col-md-4 control-label" for="textinput">Nama Agent</label>  
            <div class="col-md-8">
            <input id="textinput" name="nama_agent" type="text" placeholder="" class="form-control input-md" required> 
            </div>
          </div>

          <!-- Textarea -->
          <div class="form-group">
            <label class="col-md-4 control-label" for="textarea">Pesan</label>
            <div class="col-md-8">                     
              <textarea class="form-control" id="msg" name="msg" required ></textarea>
            </div>
          </div>

          <!-- Button -->
          <div class="form-group">
            <label class="col-md-4 control-label" for="singlebutton"></label>
            <div class="col-md-8">
              <button id="singlebutton" name="singlebutton" class="btn btn-primary">Kirim Pesan</button>
            </div>
          </div>

          </fieldset>
          </form>


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

@endsection

@section('js')
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>
<script src="{{ asset('js/sweetalert2.min.js') }}"></script>
<script src="{{ asset('js/confirm.js') }}"></script>
<script>
  $(document).ready(function() {
    $('#example').DataTable();
} );
</script>
@endsection
