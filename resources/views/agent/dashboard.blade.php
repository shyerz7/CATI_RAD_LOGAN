@extends('layouts.app')

@section('css')
<style>

.user_name{
    font-size:14px;
    font-weight: bold;
}
.comments-list .media{
  padding: 10px 0;
  border-bottom: 1px solid #eee;
  display: block;
  margin: 0;
}
.media {
  margin-top: 20px;
  margin-bottom: 20px;
}
.img-circle {
    border-radius: 50%!important;
}
.img-avatar {
  width: 38px;
  height: 38px;
}
a {
  color: #000000;
}
.top-area, .wiki-page-header {
    padding: 20px 0;
    border-bottom: 1px solid #f0f0f0;
    margin-bottom: 20px;
}
</style>
@endsection

@section('content')

    
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel-body">
                    <p class="lead">List Project</p>

                    <div class="comments-list">

                       @foreach($projects as $project)
                        
                            <div class="media">
                               <p class="pull-right"><small>
                                   <a href="{{ route('enterproject',$project->id) }}" class="btn btn-primary">Proses Survey <i class="fa fa-sign-in" aria-hidden="true"></i></a>
                               </small></p>
                                <a class="media-left" href="#">
                                  <img src="https://www.gravatar.com/avatar/asal32srwq4324234?d=identicon" class="img img-circle img-avatar">
                                </a>
                                <div class="media-body">

                                  <a href="#"><h4 class="media-heading user_name">{{ $project->name }}</h4></a>
                                  Client           : {{ $project->client_name }} <br>
                                  Target Responden : <span class="label label-primary">{{ $project->target_responden }} </span><br>
                                  Project Manager  : {{ $project->project_manajer }}
                                </div>
                           </div>

                       @endforeach     
                       
                   </div>
        
                </div>
            </div>
        </div>
    </div>



@endsection