<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Project;
use App\Models\Responden;
use App\Models\Agents;
use App\Models\Status;
use App\Models\LogResponden;
use App\Models\KuotaParameter;
use App\Models\Message;
use Session;
use Auth;
use Carbon\Carbon;
use Excel;

class AgentController extends Controller
{
    public function dashboard()
    {

    	$projects = Project::get();



    	return view('agent.dashboard',compact('projects'));
    }

    public function enterproject(Request $request,$id_project)
    {

        $datenow =   Carbon::now();

    	$project = Project::findOrFail($id_project);

    	$request->session()->put('session_project', $project);

    	$user = Auth::user();

    	$agent = Agents::where('user_id',$user->id)->first();

        $perolehanRespondenSukses = Responden::where('agent_id', $agent->id)
                                            ->where('last_status_id', 1)
                                            ->where('project_id', $project->id)
                                            ->get();
                                            //dd($perolehanRespondenSukses->count());

    	$respondens = Responden::with(['status','kuota'])
    							->where('project_id',$project->id)
    							->where('agent_id',$agent->id)
                                ->whereIn('last_status_id',[3,4,6,7,8,9,99]) // status sukses
    							->get();



    	return view('agent.listresponden2',compact('project','agent','respondens','datenow', 'perolehanRespondenSukses'));

    }

    public function showdetailrep($id_responden)
    {
    	$responden = Responden::findOrFail($id_responden);

    	$list_nomor_hp = json_decode($responden->nomer_hp);

    	$list_status = Status::where('id','<>',99)
    						   ->pluck('name','id');

        $log_respondens = LogResponden::with('status')
                                        ->where('responden_id',$responden->id)
                                        ->orderBy('id','desc')
                                        ->get();
        /**
         * update jd status
         */
        $responden->is_lock = true;

        $callcount = $responden->call_attempt;
        $callcount = $callcount+1;
        $responden->call_attempt = $callcount;

        $responden->save();

    	return view('agent.showdetailrep',compact('responden','list_nomor_hp','list_status','log_respondens'));
    }

    public function updateLockRespActive(Request $request,$id_resp)
    {
        if($request->ajax()){

            $responden = Responden::findOrFail($id_resp);

            $responden->is_lock = true;

            /**
             * call attempt prose
             */
            $callcount = $responden->call_attempt;
            $callcount = $callcount+1;
            $responden->call_attempt = $callcount;

            $responden->save();


        }
    }

    public function updateLockRespInActive(Request $request,$id_resp)
    {
        if($request->ajax()){

            $responden = Responden::findOrFail($id_resp);

            $responden->is_lock = false;
            $responden->save();

        }
    }

    public function updateStatRespOnDetailPage(Request $request,$id_resp)
    {
        $responden = Responden::findOrFail($id_resp);

        /**
         * save log responden
         */
        $data['responden_id']       =   $responden->id;
        $data['status_id']          =   $request->input('status_id');
        $data['nomor_hp']           =   $request->input('nomer_hp');
        $data['agent_name']         =   $request->input('agent_name');

        if($request->has('note'))  {
            $data['note']  =   $request->input('note');
        }

        /**
         * tgl reschedule
         */

        $tgl_reschedule = date('Y-m-d H:i:s', strtotime($request->input('datetimereschedule')));

        if($tgl_reschedule == '1970-01-01 00:00:00') {
             $tgl_reschedule = null;
        }

        $data['tgl_reschedule']  =  $tgl_reschedule;

        $log = LogResponden::create($data);

        /**
         * update responden
         */
        $responden->last_status_id  =  $request->input('status_id');
        $responden->is_lock = false;
        $responden->agent_name = $request->input('agent_name');
        $responden->save();

        $project = Session::get('session_project');

        return redirect()->route('enterproject',$project->id);
    }

    public function onsurvey($id_resp)
    {
        $responden = Responden::findOrFail($id_resp);

        $list_status = Status::whereIn('id',[1,2,4,6])
                               ->pluck('name','id');

        return view('agent.onsurvey',compact('responden','list_status'));
    }

    public function onsurveyProcess(Request $request,$id_resp)
    {
        if($request->ajax()){

            $responden = Responden::findOrFail($id_resp);

            return view('agent.ajaxloadlinksurvey',compact('responden'));

        }
    }

    public function ajaxCheckKuota(Request $request, $id_responden)
    {
        if($request->ajax()){

            $responden = Responden::findOrFail($id_responden);

            $kuota = KuotaParameter::findOrFail($responden->kuota_parameter_id);

            $perolehanResp = Responden::where('kuota_parameter_id',$kuota->id)
                                         ->where('last_status_id',1)
                                         ->count();

            if($perolehanResp >= $kuota->kuota) {
                return "ok";
            }

        }
    }
    public function perolehanRespByAgent($id_agent)
    {
    	$agent = Agents::findOrFail($id_agent);

    	$respondens = Responden::with('status')->where('agent_id',$agent->id)
                   ->where('last_status_id', 1)
    							 ->orderBy('updated_at','desc')
    							 ->get();

    	ob_end_clean();

        Excel::create('Template Upload Responden',function($excel) use ($respondens){

            //set the properties
            $excel->setTitle('List Responden')
                ->setCreator('Teten')
                ->setCompany('RAD')
                ->setDescription('List Perolehan Responden');

            $excel->sheet('List Perolehan',function($sheet)  use ($respondens) {

        		$sheet->row(1,array('No','Agent Caller','Resp ID','Status','First Name','Last Name','Email','Call Attempt','Nomor HP','Link Survey'));

        		//loop
        		foreach ($respondens as $key => $value) {
                    $sheet->row($key + 2, array(
                        $key + 1,
                        $value->agent->code,
                        $value->resp_id,
                        $value->status->name,
                        $value->first_name,
                        $value->last_name,
                        $value->email,
                        $value->call_attempt,
                        $value->nomer_hp,
                        $value->link_survey
                    ));
                }

            });
        })->export('xlsx');
    }

    public function sendMessage(Request $request, $id_agent)
    {
        $agent = Agents::findOrFail($id_agent);

        $project = Session::get('session_project');

        $data['agent_id']       =   $agent->id;
        $data['nama_agent']     =   $request->input('nama_agent');
        $data['msg']     =   $request->input('msg');
        $val = Message::create($data);

        return redirect()->route('enterproject',$project->id)->with('alert-success','Pesan berhasil dikirim');

    }
}
