<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Status;
use App\Models\Responden;
use App\Models\Agents;
use App\Models\KuotaParameter;
use Excel;
use Session;
use Carbon\Carbon;

class PerolehanController extends Controller
{
    public function index()
    {
        $project = Session::get('session_project');

        $kuotas = KuotaParameter::get();

    	$statuses = Status::orderBy('id','asc')->get();

        $listStatus = Status::pluck('name','id');

    	$agents = Agents::where('project_id',$project->id)->get();

        $respondens = Responden::with(['kuota','status'])
                                ->where('project_id',$project->id)
                                ->where('last_status_id','!=',99)
                                ->where('is_lock',false)
                                ->get();

                                // dd($respondens->toArray());

    	return view('backend.perolehan.index',compact('statuses','agents','kuotas','listStatus','respondens'));
    }

    public function perolehanByTanggal() {
        $project = Session::get('session_project');


        return view('backend.perolehan.byTanggal',compact('project'));
    }

    public function perolehanbyKuota() {
        $project = Session::get('session_project');

        $kuotas = KuotaParameter::where('project_id', $project->id)->get();

        $statuses = Status::orderBy('id','asc')->get();

        $listStatus = Status::pluck('name','id');

        $agents = Agents::where('project_id',$project->id)->get();

        $respondens = Responden::with(['kuota','status'])
            ->where('project_id',$project->id)
            ->where('last_status_id','!=',99)
            ->where('is_lock',false)
            ->get();

        // dd($respondens->toArray());

        return view('backend.perolehan.byKuota',compact('statuses','agents','kuotas','listStatus','respondens'));
    }

    public function perolehanbyStatus() {
        $project = Session::get('session_project');

        $kuotas = KuotaParameter::where('project_id', $project->id)->get();

        $statuses = Status::orderBy('id','asc')->get();

        $listStatus = Status::pluck('name','id');

        $agents = Agents::where('project_id',$project->id)->get();

        $respondens = Responden::with(['kuota','status'])
            ->where('project_id',$project->id)
            ->where('last_status_id','!=',99)
            ->where('is_lock',false)
            ->get();

        // dd($respondens->toArray());

        return view('backend.perolehan.byStatus',compact('statuses','agents','kuotas','listStatus','respondens'));
    }

    public function perolehanbyAgent() {
        $project = Session::get('session_project');


        $agents = Agents::where('project_id',$project->id)->get();

        return view('backend.perolehan.byAgent',compact('agents'));
    }

    public function perolehanRespByStatus($id_status)
    {
        $project = Session::get('session_project');

    	$status = Status::findOrFail($id_status);

    	$respondens = Responden::with(['agent','status'])
                                 ->where('project_id',$project->id)
                                 ->where('last_status_id',$status->id)
    							 ->orderBy('updated_at','desc')
    							 ->get();

    	ob_end_clean();

        Excel::create('List Responden by Status',function($excel) use ($respondens){

            //set the properties
            $excel->setTitle('List Responden')
                ->setCreator('Teten')
                ->setCompany('RAD')
                ->setDescription('List Perolehan Responden');

            $excel->sheet('List Perolehan',function($sheet)  use ($respondens) {

        		$sheet->row(1,array('No','Agent ID','Kanwil','Cabang','Kategori','Nama Agent','Resp ID','Status','First Name','Last Name','Email','Call Attempt','Nomor HP','Last Call','Link Survey'));

        		//loop
        		foreach ($respondens as $key => $value) {

                    $tglLastCall = Carbon::parse($value->updated_at);

                    $agent_id = $value->agent?$value->agent->code : '-';

                    $sheet->row($key + 2, array(
                        $key + 1,
                        $agent_id,
                        $value->kanwil,
                        $value->cabang,
                        $value->kategori,
                        $value->agent_name,
                        $value->resp_id,
                        $value->status->name,
                        $value->first_name,
                        $value->last_name,
                        $value->email,
                        $value->call_attempt,
                        $value->nomer_hp,
                        $tglLastCall,
                        $value->link_survey
                    ));
                }

            });
        })->export('xlsx');
    }

    public function perolehanRespByAgent($id_agent)
    {
        $project = Session::get('session_project');

    	$agent = Agents::findOrFail($id_agent);

    	$respondens = Responden::with('status')->where('agent_id',$agent->id)
    							 ->orderBy('updated_at','desc')
    							 ->get();

    	ob_end_clean();

        Excel::create('List Responden By Agent',function($excel) use ($respondens){

            //set the properties
            $excel->setTitle('List Responden')
                ->setCreator('Teten')
                ->setCompany('RAD')
                ->setDescription('List Perolehan Responden');

            $excel->sheet('List Perolehan',function($sheet)  use ($respondens) {

        		$sheet->row(1,array('No','Agent ID','Kanwil','Cabang','Kategori','Nama Agent','Resp ID','Status','First Name','Last Name','Email','Call Attempt','Nomor HP','Last Call','Link Survey'));

        		//loop
        		foreach ($respondens as $key => $value) {

                    $tglLastCall = Carbon::parse($value->updated_at);

                    $sheet->row($key + 2, array(
                        $key + 1,
                        $value->agent->code,
                        $value->kanwil,
                        $value->cabang,
                        $value->kategori,
                        $value->agent_name,
                        $value->resp_id,
                        $value->status->name,
                        $value->first_name,
                        $value->last_name,
                        $value->email,
                        $value->call_attempt,
                        $value->nomer_hp,
                        $tglLastCall,
                        $value->link_survey
                    ));
                }

            });
        })->export('xlsx');
    }

    public function perolehanRespByKuota($id_kuota)
    {
        $project = Session::get('session_project');

        $kuota = KuotaParameter::findOrFail($id_kuota);

        $respondens = Responden::with('status')
                                 ->where('project_id',$project->id)
                                 ->where('kuota_parameter_id',$kuota->id)
                                 ->where('last_status_id',1)
                                 ->orderBy('updated_at','desc')
                                 ->get();

        ob_end_clean();

        Excel::create('List Responden by Kuota',function($excel) use ($respondens){

            //set the properties
            $excel->setTitle('List Responden')
                ->setCreator('Teten')
                ->setCompany('RAD')
                ->setDescription('List Perolehan Responden');

            $excel->sheet('List Perolehan',function($sheet)  use ($respondens) {

                $sheet->row(1,array('No','Agent ID','Kanwil','Cabang','Kategori','Nama Agent','Parameter','Resp ID','Status','First Name','Last Name','Email','Call Attempt','Nomor HP','Tgl Cal','Link Survey'));

                //loop
                foreach ($respondens as $key => $value) {

                    $tglLastCall = Carbon::parse($value->updated_at);

                    $sheet->row($key + 2, array(
                        $key + 1,
                        $value->agent->code,
                        $value->kanwil,
                        $value->cabang,
                        $value->kategori,
                        $value->agent_name,
                        $value->kuota->nama_parameter,
                        $value->resp_id,
                        $value->status->name,
                        $value->first_name,
                        $value->last_name,
                        $value->email,
                        $value->call_attempt,
                        $value->nomer_hp,
                        $tglLastCall,
                        $value->link_survey
                    ));
                }

            });
        })->export('xlsx');
    }

    public function perolehanRespByDate(Request $request)
    {
        if($request->ajax())
        {
            $project = Session::get('session_project');

            $dateFrom = $request->get('dateFrom');
            $dateTo = $request->get('dateTo');

            /**
             * date convert to carbon
             */
            $convDateFrom = Carbon::parse($dateFrom);
            $convDateTo   = Carbon::parse($dateTo);

            $status_id = $request->get('status_id');
            if($status_id == 100) {
                $respondens = Responden::with('agent')
                                 ->where('project_id',$project->id)
                                 ->whereNotNull('agent_id')
                                 ->where('last_status_id','!=',99)
                                 ->whereBetween('updated_at', array($convDateFrom, $convDateTo))
                                 ->get();
            }else{


                $status = Status::findOrFail($status_id);

                $respondens = Responden::with('agent')
                                 ->where('project_id',$project->id)
                                 ->where('last_status_id',$status_id)
                                 ->whereBetween('updated_at', array($convDateFrom, $convDateTo))
                                 ->get();
            }


            return view('backend.perolehan.ajaxGetResp',compact('respondens','status','project','convDateFrom','convDateTo','status_id','dateFrom','dateTo'));

        }
    }

    public function perolehanRespByDateExport(Request $request)
    {
        // if($request->ajax())
        // {

            $dateFrom = $request->get('dateFrom');
            $dateTo = $request->get('dateTo');
            $status_id = $request->get('status_id');





            $project = Session::get('session_project');

             /**
             * date convert to carbon
             */
            $convDateFrom = Carbon::parse($dateFrom);
            $convDateTo   = Carbon::parse($dateTo);

            if($status_id != '100') {
                $status = Status::findOrFail($status_id);
                $respondens = Responden::with(['status'])
                                 ->where('project_id',$project->id)
                                 ->where('last_status_id',$status_id)
                                 ->whereBetween('updated_at', array($convDateFrom, $convDateTo))
                                 ->get();
            }else {
                $respondens = Responden::with(['status'])
                                 ->where('project_id',$project->id)
                                 ->whereNotNull('agent_id')
                                 ->where('last_status_id','!=',99)
                                 ->whereBetween('updated_at', array($convDateFrom, $convDateTo))
                                 ->get();

            }



            ob_end_clean();

            Excel::create('Perolehan Responden',function($excel) use ($respondens){

                //set the properties
                $excel->setTitle('List Responden')
                    ->setCreator('Teten')
                    ->setCompany('RAD')
                    ->setDescription('List Perolehan Responden');

                $excel->sheet('List Perolehan',function($sheet)  use ($respondens) {

                    $sheet->row(1,array('No','Agent ID','Kanwil','Cabang','Kategori','Nama Agent','Resp ID','Status','First Name','Last Name','Email','Call Attempt','Nomor HP','Last Call','Link Survey'));

                    //loop
                    foreach ($respondens as $key => $value) {

                        $tglLastCall = Carbon::parse($value->updated_at);

                        $sheet->row($key + 2, array(
                            $key + 1,
                            $value->agent->code,
                            $value->kanwil,
                            $value->cabang,
                            $value->kategori,
                            $value->agent_name,
                            $value->resp_id,
                            $value->status->name,
                            $value->first_name,
                            $value->last_name,
                            $value->email,
                            $value->call_attempt,
                            $value->nomer_hp,
                            $tglLastCall,
                            $value->link_survey
                        ));
                    }

                });
            })->export('xlsx');

        // }
    }
}
