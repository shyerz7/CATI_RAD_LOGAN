<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Project;
use Session;

class DashboardController extends Controller
{
    public function dashboard()
    {

    	$projects = Project::all();

    	return view('backend.dashboard.dashboard',compact('projects'));
    }


    public function redirect(Request $request,$id)
    {
    	$project = Project::findOrFail($id);

    	$request->session()->put('session_project', $project);

    	return redirect('/admin/access/agents');
    }

    

    
}
