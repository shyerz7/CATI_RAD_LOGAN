<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Agents;
use Session;
use App\Http\Requests\AgentRequest;
use App\Role;
use App\User;

class AgentController extends Controller
{
    public function index() 
    {
    	$project = Session::get('session_project');

    	$agents = Agents::with('user')->where('project_id',$project->id)->get();

        // dd($agents->toArray());

    	return view('backend.agent.index',compact('agents','project'));

    }

    public function create()
    {
    	return view('backend.agent.create');
    }

    public function store(AgentRequest $request)
    {
    	$data['name']			=	$request->input('name');
    	$data['code']			=	$request->input('code');
    	$data['alamat']			=	$request->input('alamat');
    	$data['hp']				=	$request->input('hp');
    	$data['project_id']		=	Session::get('session_project')->id;
    	$agent = Agents::create($data);


    	/**
    	 * user agent
    	 */
    	$role = Role::where('name','agent')->first();

    	$user = new User();
    	$user->name 	= $request->input('name');
    	$user->email 	= $request->input('email');
    	$user->password = bcrypt($request->input('password'));
    	$user->save();


    	$user->attachRole($role);

    	$agent->user_id = $user->id;
    	$agent->save();

    	return redirect()->route('agents.index')->with('alert-success','Berhasil menyimpan data agent');

    }

    public function edit($id)
    {
    	$agent = Agents::with('user')->findOrFail($id);

    	return view('backend.agent.edit',compact('agent'));
    }

    public function update(AgentRequest $request,$id)
    {
    	$agent = Agents::findOrFail($id);

    	$agent->name 	= $request->input('name');
    	$agent->code 	= $request->input('code');
    	$agent->alamat 	= $request->input('alamat');
    	$agent->hp 		= $request->input('hp');
    	$agent->save();

    	$user = User::findOrFail($agent->user_id);

        if($request->has('email'))
        {
        	$user->email = $request->input('email');
        	$user->save();
        }

        if($request->has('password'))
        {
        	$user->password = bcrypt($request->input('password'));
        	$user->save();
        }

        return redirect()->route('agents.index')->with('alert-success','Berhasil mengupdate data agent');

    }

    public function destroy($id)
    {
    	Agents::destroy($id);
    }
}
