<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Message;

class MessageController extends Controller
{
    public function index()
    {
    	$messages = Message::with('agent')->where('is_read',false)->orderBy('created_at','desc')->get();

    	return view('backend.message.index',compact('messages'));

    }

    public function edit($id_pesan)
    {
    	$message = Message::findOrFail($id_pesan);
    	$message->is_read = true;
    	$message->save();

    	 return redirect()->route('pesan.index')->with('alert-success','Masalah berhasil ditangani');
    }
}
