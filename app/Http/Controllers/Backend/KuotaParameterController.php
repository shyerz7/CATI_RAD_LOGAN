<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\KuotaParameter;
use Session;
use Excel;
use Validator;

class KuotaParameterController extends Controller
{
    public function index() {

    	$project = Session::get('session_project');

    	$kuotas = KuotaParameter::where('project_id',$project->id)->get();

    	return view('backend.kuota.index',compact('project','kuotas'));

    }

    public function create() {
    	return view('backend.kuota.create');
    }

    public function store(Request $request) {

    	$data['project_id']	    = \Session::get('session_project')->id;
    	$data['nama_parameter']	= $request->input('nama_parameter');
    	$data['kuota']			= $request->input('kuota');

    	$value = KuotaParameter::create($data);

    	return redirect()->route('kuotas.index')->with('alert-success','Berhasil menyimpan data kuota');
    }

    public function edit($id_kuota) {

    	$kuota = KuotaParameter::findOrFail($id_kuota);

    	return view('backend.kuota.edit',compact('kuota'));
    }

    public function update(Request $request,$id)
    {
    	$kuota = KuotaParameter::findOrFail($id);

    	$kuota->nama_parameter 	=	$request->input('nama_parameter');
    	$kuota->kuota 			=	$request->input('kuota');
    	$kuota->save();

    	return redirect()->route('kuotas.index')->with('alert-success','Berhasil menyimpan data kuota');

    }

    public function destroy($id)
    {
    	KuotaParameter::destroy($id);
    }

    public function generateExcelTemplate()
    {

        ob_end_clean();  
        
        Excel::create('Template Upload Data Kuota',function($excel){

            //set the properties
            $excel->setTitle('Template Upload Kuota')
                ->setCreator('Teten')
                ->setCompany('RAD')
                ->setDescription('Template untuk upload data repsonden projek MS');

            $excel->sheet('Data Responden',function($sheet){
                $column = [];
                array_push($column,'nama_parameter');
                array_push($column,'kuota');

                $row = 1;
                $sheet->row($row,$column);
            });
        })->export('xlsx');
    }

    public function importExcel(Request $request) 
    {

        // validasi untuk memastikan file yang diupload adalah excel
        $this->validate($request, [ 'excel' => 'required|mimes:xls,xlsx' ]);

        // ambil file yang baru diupload
        $excel = $request->file('excel');

        // baca sheet pertama
        $excels = Excel::selectSheetsByIndex(0)->load($excel, function($reader) {
            // options, jika ada
        })->get();

        //rule untuk validasi setiap row pada file excel
        $rowRules = [
            'nama_parameter'     =>  'required',
            'kuota'              =>  'required',
           
        ];

         // Catat semua id buku baru
        // ID ini kita butuhkan untuk menghitung total buku yang berhasil diimport
        $kuotas_id = [];

        $project = Session::get('session_project');

        //looping setiap baris, mulai dari baris ke 2 (karena baris ke 1 adalah nama kolom)
        foreach($excels as $row) {

            //Membuat vlidasi untuk row di excel
            // dsini kita ubah baris yang sedang diproses menjadi array
            $validator = Validator::make($row->toArray(),$rowRules);

            // skip baris ini jika tidak valid, langsung ke baris selajutnya
            if ($validator->fails()) continue;

            // credentials data
            $kuota = KuotaParameter::where('project_id',$project->id)
                                     ->where('nama_parameter',$row['nama_parameter']  )
                                     ->first();   

            if(!is_null($kuota)) {
                $kuota->kuota          =    (int)$row['kuota'] ;
                $kuota->save();    
            }else{
                $data['project_id']         =   $project->id;
                $data['nama_parameter']  =   $row['nama_parameter'];
                $data['kuota']  =   $row['kuota'];
                $kuota = KuotaParameter::create($data);
            }                                  

            array_push($kuotas_id,$kuota->id);

        }// end foreach

        // Ambil semua staff yang baru dibuat
        $kuotas = KuotaParameter::whereIn('id',$kuotas_id)->get();

        if ($kuotas->count() == 0) {

            return redirect()->route('kuotas.index')->with('flash_notification',"Gagal mengimport responden. Cek kembali data inputan Anda");
        }

        // set feedback
        return redirect()->route('kuotas.index')->with('alert-success',"Berhasil mengimport " . $kuotas->count() . " kuota.");

    }

}
