<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Responden;
use App\Models\ListRekaman;
use App\Models\Project;
use Session;
use Excel;
use Validator;
use Carbon\Carbon;

class ListRekamanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $project = Session::get('session_project');

        $respondens = Responden::with(['kuota','status','agent','rekaman'])
                                 ->where('last_status_id',1)
                                 ->where('project_id',$project->id)
                                 ->get();

        return view('backend.listrekaman.index',compact('respondens','project'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function generateExcelTemplate()
    {

        ob_end_clean();  
        
        Excel::create('Template Upload Rekaman',function($excel){

            //set the properties
            $excel->setTitle('Template Upload Responden')
                ->setCreator('Teten')
                ->setCompany('RAD')
                ->setDescription('Template untuk upload data rekaman projek MS');

            $excel->sheet('Data Rekaman ',function($sheet){
                $column = [];
                array_push($column,'nama_rekaman');
                array_push($column,'tanggal');

                $row = 1;
                $sheet->row($row,$column);
            });
        })->export('xlsx');
    }

    public function playrekaman(Request $request)
    {
        if($request->ajax()){
            $id=$request->input('id');
            $responden = Responden::findOrFail($id);
            $listRekamans = ListRekaman::where('resp_id',$responden->id)->get();
            return view('backend.listrekaman.ajax_play_rekaman',compact('responden','listRekamans'));
        }
    }

    public function importExcel(Request $request)
    {
        // validasi untuk memastikan file yang diupload adalah excel
        $this->validate($request, [ 'excel' => 'required|mimes:xls,xlsx' ]);

        // ambil file yang baru diupload
        $excel = $request->file('excel');

        // baca sheet pertama
        $excels = Excel::selectSheetsByIndex(0)->load($excel, function($reader) {
            // options, jika ada
        })->get();

        //rule untuk validasi setiap row pada file excel
        $rowRules = [
            'nama_rekaman'     =>  'required',
            'tanggal'     =>  'required',
           
        ];

         // Catat semua id buku baru
        // ID ini kita butuhkan untuk menghitung total buku yang berhasil diimport
        $rekamans_id = [];

        $project = Session::get('session_project');

        //looping setiap baris, mulai dari baris ke 2 (karena baris ke 1 adalah nama kolom)
        foreach($excels as $row) {

            //Membuat vlidasi untuk row di excel
            // dsini kita ubah baris yang sedang diproses menjadi array
            $validator = Validator::make($row->toArray(),$rowRules);

            // skip baris ini jika tidak valid, langsung ke baris selajutnya
            if ($validator->fails()) continue;

            $temp = explode("-",$row["nama_rekaman"]);

            $resp_id = $temp[0];

            //cek
            $responden = Responden::where('resp_id',$resp_id)->first();

            if(!is_null($responden)) {
                $data['resp_id']        =  $responden->id;
                $data['nama_rekaman']   =  $row["nama_rekaman"].'.mp3';
                $data['tgl_call']       =  Carbon::parse($row["tanggal"]);

                $val = ListRekaman::create($data);

                array_push($rekamans_id,$val->id);

            }
            
        }// end foreach

        // Ambil semua staff yang baru dibuat
        $rekamans = ListRekaman::whereIn('id',$rekamans_id)->get();

        if ($rekamans->count() == 0) {

            return redirect()->route('listrekaman.index')->with('flash_notification',"Gagal mengimport responden. Cek kembali data inputan Anda");
        }

        // set feedback
        return redirect()->route('listrekaman.index')->with('alert-success',"Berhasil mengimport " . $rekamans->count() . " responden.");
    }

    public function ajaxListRespByStatus (Request $request,$id_project,$id_status) 
    {
        if($request->ajax())
        {
            $project = Project::findOrFail($id_project);

            $respondens = Responden::with('status')
                                     ->where('project_id',$project->id)
                                     ->where('last_status_id',$id_status)
                                     ->get();


            return view('client.ajaxtable',compact('project','respondens'));
                                            
        }
    }

}
