<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use App\Models\Responden;
use App\Models\KuotaParameter;
use Excel;
use Validator;
use Yajra\Datatables\Facades\Datatables;
use Carbon\Carbon;

class RespondenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $project = Session::get('session_project');

        // dd($project->toArray());

        // $respondens = Responden::where('project_id',$project->id)
        //                          ->with('agent')
        //                          ->orderBy('id','desc')
        //                          ->get();
                                 


        $kuotas = KuotaParameter::where('project_id',$project->id)->get();                         

        return view('backend.responden.index',compact('project','respondens','kuotas'));                            

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kuotas = KuotaParameter::pluck('nama_parameter','id');

        return view('backend.responden.create',compact('kuotas'));                
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $project = Session::get('session_project');
        $data['project_id']             =   $project->id;
        $data['kuota_parameter_id']     =   $request->input('kuota_parameter_id');
        $data['resp_id']        =   $request->input('resp_id');
        $data['first_name']     =   $request->input('first_name');
        $data['last_name']      =   $request->input('last_name');
        $data['email']          =   $request->input('email');
        $data['link_survey']    =   $request->input('link_survey');
        $data['link_expired']   =   date('Y-m-d', strtotime($request->input('link_expired'))); 
        $data['last_status_id'] =    99;
        $responden = Responden::create($data);

        //update nomer hp
        if($request->has('teks'))
        {
            $text = $request->input('teks');
            $array_nomer_hp = [];

            foreach($text as $value)
            {
                array_push($array_nomer_hp,$value);
            }

            // simpan ke db
            $responden->nomer_hp = json_encode($array_nomer_hp);
            $responden->save();

        }

        return redirect()->route('respondens.index')->with('alert-success','Tambah responden berhasil');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $kuotas = KuotaParameter::pluck('nama_parameter','id');

        $respondent = Responden::findOrFail($id);

        return view('backend.responden.edit',compact('respondent','kuotas'));        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $respondent = Responden::findOrFail($id);

        $respondent->kuota_parameter_id        =   $request->input('kuota_parameter_id');
        $respondent->resp_id        =   $request->input('resp_id');
        $respondent->first_name     =   $request->input('first_name');
        $respondent->last_name      =   $request->input('last_name');
        $respondent->email          =   $request->input('email');
        $respondent->link_survey    =   $request->input('link_survey');
        $respondent->link_expired   =   date('Y-m-d', strtotime($request->input('link_expired'))); 

        $respondent->save();

        return redirect()->route('respondens.index')->with('alert-success','Update responden berhasil');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Responden::destroy($id);
    }

    public function generateExcelTemplate()
    {

        ob_end_clean();  
        
        Excel::create('Template Upload Responden',function($excel){

            //set the properties
            $excel->setTitle('Template Upload Responden')
                ->setCreator('Teten')
                ->setCompany('RAD')
                ->setDescription('Template untuk upload data repsonden projek MS');

            $excel->sheet('Data Responden',function($sheet){
                $column = [];
                array_push($column,'kuota_parameter_id');
                array_push($column,'resp_id');
                array_push($column,'first_name');
                array_push($column,'last_name');
                array_push($column,'email');
                array_push($column,'link_survey');
                array_push($column,'link_expired');
                array_push($column,'nomer_hp_1');
                array_push($column,'nomer_hp_2');
                array_push($column,'nomer_hp_3');

                $row = 1;
                $sheet->row($row,$column);
            });
        })->export('xlsx');
    }

    public function importExcel(Request $request)
    {
        // validasi untuk memastikan file yang diupload adalah excel
        $this->validate($request, [ 'excel' => 'required|mimes:xls,xlsx' ]);

        // ambil file yang baru diupload
        $excel = $request->file('excel');

        // baca sheet pertama
        $excels = Excel::selectSheetsByIndex(0)->load($excel, function($reader) {
            // options, jika ada
        })->get();

        //rule untuk validasi setiap row pada file excel
        $rowRules = [
            'kuota_parameter_id'     =>  'required',
            'resp_id'     =>  'required',
            'first_name'  =>  'required',
            'nomer_hp_1'  =>  'required',
           
        ];

         // Catat semua id buku baru
        // ID ini kita butuhkan untuk menghitung total buku yang berhasil diimport
        $respondens_id = [];

        $project = Session::get('session_project');

        //looping setiap baris, mulai dari baris ke 2 (karena baris ke 1 adalah nama kolom)
        foreach($excels as $row) {

            //Membuat vlidasi untuk row di excel
            // dsini kita ubah baris yang sedang diproses menjadi array
            $validator = Validator::make($row->toArray(),$rowRules);

            // skip baris ini jika tidak valid, langsung ke baris selajutnya
            if ($validator->fails()) continue;

            // credentials data
            $responden = Responden::firstOrCreate([
                                        'project_id'    =>  $project->id,
                                        'resp_id'       =>  $row['resp_id'],    
                                        'kuota_parameter_id'       =>  $row['kuota_parameter_id']    
                                    ]);

            $responden->first_name              =    $row['first_name'];
            $responden->last_name               =    $row['last_name'];
            $responden->email                   =    $row['email'];
            $responden->link_survey             =    $row['link_survey'];
            $responden->link_expired            =    date('Y-m-d', strtotime($row['link_expired']));
            $responden->last_status_id          =    99;

            //update nomer hp
            $array_nomer_hp = [];
            if($row['nomer_hp_1'])
            {
                array_push($array_nomer_hp,$row['nomer_hp_1']);
            }
            if($row['nomer_hp_2'])
            {
                array_push($array_nomer_hp,$row['nomer_hp_2']);
            }
            if($row['nomer_hp_3'])
            {
                array_push($array_nomer_hp,$row['nomer_hp_3']);
            }

            // simpan ke db
            $responden->nomer_hp = json_encode($array_nomer_hp);

            // dd($responden->toArray());

            $responden->save();

            array_push($respondens_id,$responden->id);

            
        }// end foreach

        // Ambil semua staff yang baru dibuat
        $respondens = Responden::whereIn('id',$respondens_id)->get();

        if ($respondens->count() == 0) {

            return redirect()->route('respondens.index')->with('flash_notification',"Gagal mengimport responden. Cek kembali data inputan Anda");
        }

        // set feedback
        return redirect()->route('respondens.index')->with('alert-success',"Berhasil mengimport " . $respondens->count() . " responden.");

    }

    public function respondensApi(Request $request)
    {
        if($request->ajax()){
            $project = Session::get('session_project');

            $respondens = Responden::where('project_id',$project->id)
                             // ->with('agent')
                             ->orderBy('id','desc')
                             ->get();
                             
            return Datatables::of($respondens)
                ->addColumn('action', function ($responden)
                {
                    return
                        '<div class="btn-group">' .
                            '<a href="'. route('respondens.edit', [$responden->id]) .'" class="btn btn-warning btn-xs" data-toggle="tooltip" data-original-title="Rubah" data-container="body">'.
                            '   <i class="fa fa-edit"></i>'.
                            '</a>'.
                            '<a href="#" data-target="'. route('respondens.destroy', [$responden->id]) .'" class="btn btn-danger btn-xs confirmation" data-toggle="tooltip" data-original-title="Hapus" data-container="body" onclick="hapusData(this)">'.
                            '<i class="fa fa-trash-o"></i>'.
                            '</a>'.
                        '</div>';
                })
                ->editColumn('id', 'ID: {{$id}}')
                ->make(true);      
        }                     
    }

    public function exportRespondens()
    {
        $project = Session::get('session_project');

        $respondens = Responden::with(['agent','status'])
                                 ->where('project_id',$project->id)   
                                 ->where('last_status_id',3)   
                                 ->orderBy('updated_at','desc')
                                 ->get();

        ob_end_clean();

        Excel::create('Responden List',function($excel) use ($respondens){

            //set the properties
            $excel->setTitle('List Responden')
                ->setCreator('Teten')
                ->setCompany('RAD')
                ->setDescription('List Perolehan Responden');

            $excel->sheet('List Perolehan',function($sheet)  use ($respondens) {

                $sheet->row(1,array('No','Agent ID','Nama Agent','Resp ID','Status','First Name','Last Name','Email','Call Attempt','Nomor HP','Last Call','Link Survey'));

                //loop
                foreach ($respondens as $key => $value) {

                    $tglLastCall = Carbon::parse($value->updated_at);

                    $agent_id = $value->agent?$value->agent->code : '-';

                    $sheet->row($key + 2, array(
                        $key + 1,
                        $agent_id,
                        $value->agent_name,
                        $value->resp_id,
                        $value->status->name,
                        $value->first_name,
                        $value->last_name,
                        $value->email,
                        $value->call_attempt,
                        $value->nomer_hp,
                        $tglLastCall,
                        $value->link_survey
                    ));
                }

            });
        })->export('xlsx');
    }
}