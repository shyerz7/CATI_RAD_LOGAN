<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Agents;
use App\Models\Responden;
use App\Models\KuotaParameter;
use App\Models\LogResponden;
use App\Models\Status;
use Session;

class AssignmentController extends Controller
{
    public function kuota()
    {
        $project = Session::get('session_project');

        $kuotas = KuotaParameter::where('project_id',$project->id)->get();

        return view('backend.assignment.kuota',compact('kuotas'));
    }

    public function index($id_kuota)
    {

        $kuota = KuotaParameter::findOrFail($id_kuota);

    	$project = Session::get('session_project');

    	$agents = Agents::with('respondens')->where('project_id',$project->id)->get();					

    	return view('backend.assignment.index',compact('project','agents','kuota'));
    }

    public function assign($id_agent,$id_kuota)
    {
        $kuota = KuotaParameter::findOrFail($id_kuota);

    	$project = Session::get('session_project');

    	$agent = Agents::findOrFail($id_agent);

    	$respondens = Responden::with('kuota')
                                ->where('project_id',$project->id)
    							->whereNull('agent_id')
    							->where('last_status_id',99)
                                ->where('is_lock',false)
                                ->where('kuota_parameter_id',$kuota->id)
    							->get();


    	return view('backend.assignment.assign',compact('agent','respondens','kuota'));
    }

    public function assignProcess(Request $request,$id_agent,$id_kuota)
    {
    	$agent = Agents::findOrFail($id_agent);
        $kuota = KuotaParameter::findOrFail($id_kuota);

    	$respondens = $request->input('respondens');

    	if(count($respondens))
    	{
    		foreach ($respondens as $id_responden ) {
    			// $responden isinya id responden	
    			$responden = Responden::findOrFail($id_responden);
    			$responden->agent_id = $agent->id;
    			$responden->save();		
    		}

    		return redirect()->route('assignment.index',$kuota->id)->with('alert-success','Berhasil mengassignt responden');
    	}else{
    		return redirect()->route('assignment.index',$kuota->id)->with('alert-gagal','Gagal  mengassignt responden');
    	}



    }

    public function assignTransfer($id_agent,$id_kuota)
    {
    	$project = Session::get('session_project');

    	$agent = Agents::findOrFail($id_agent);

        $kuota = KuotaParameter::findOrFail($id_kuota);

    	$respondens = Responden::with('status')->where('project_id',$project->id)
    							->where('agent_id',$agent->id)
    							->where('is_lock',false)
    							->where('last_status_id',99)
                                ->where('kuota_parameter_id',$kuota->id)
    							->get();

    	$list_agents = 	Agents::where('id','<>',$agent->id)
                                ->where('project_id',$project->id)     
    							->pluck('name','id');						

    	return view('backend.assignment.transfer',compact('project','agents','agent','respondens','list_agents','kuota'));							

    }

    public function transferProcess(Request $request,$id_agent,$id_kuota)
    {
    	$agent = Agents::findOrFail($id_agent);

    	$agent_tujuan = $request->input('agent_id');


    	$respondens = $request->input('respondens');

        $kuota = KuotaParameter::findOrFail($id_kuota);

    	if(count($respondens))
    	{
    		foreach ($respondens as $id_responden ) {
    			// $responden isinya id responden	
    			$responden = Responden::findOrFail($id_responden);
    			$responden->agent_id = $agent_tujuan;
    			$responden->save();		
    		}

    		return redirect()->route('assignment.index', $kuota->id)->with('alert-success','Berhasil trasfer responden');
    	}else{
    		return redirect()->route('assignment.index',$kuota->id)->with('alert-gagal','Gagal  transfer responden');
    	}
    }

    public function viewResp($id_agent,$id_kuota)
    {
        $agent = Agents::findOrFail($id_agent);

        $project = Session::get('session_project');

        $kuota = KuotaParameter::findOrFail($id_kuota);


        $respondens = Responden::with(['kuota','status'])
                                ->where('project_id',$project->id)
                                ->where('agent_id',$agent->id)
                                ->where('is_lock',false)
                                ->where('kuota_parameter_id',$kuota->id)
                                ->get();

        return view('backend.assignment.viewResp',compact('agent','project','kuota','respondens'));                          
                                
    }

    public function viewRespDetail($id_agent,$id_kuota,$id_resp)
    {
        $project = Session::get('session_project');

        $kuota = KuotaParameter::findOrFail($id_kuota);

        $agent = Agents::findOrFail($id_agent);

        $respondent = Responden::with(['kuota','status'])->findOrFail($id_resp);

        $list_status = Status::pluck('name','id');

        return view('backend.assignment.viewRespDetail',compact('agent','project','kuota','respondent','list_status'));   


    }

    public function viewRespDetailUpdate(Request $request,$id_resp)
    {
        $project = Session::get('session_project');


        $respondent = Responden::findOrFail($id_resp);
        $respondent->last_status_id = $request->input('status_id');
        $respondent->save();
        // dd($respondent->toArray());    
        /**
         * get log terakhir
         */
        // $logs = LogResponden::where('responden_id',$respondent->id)->delete();
        $last_log  = LogResponden::where('responden_id',$respondent->id)
                                  ->orderBy('id','desc')
                                  ->first();

        if(!is_null($last_log))
        {
            $new_log                = new LogResponden();
            $new_log->responden_id  = $last_log->responden_id;
            $new_log->status_id     = $request->input('status_id');
            $new_log->nomor_hp      = $last_log->nomor_hp;
            $new_log->tgl_reschedule  = $last_log->tgl_reschedule;
            $new_log->agent_name   = $last_log->agent_name;
            $new_log->save();

        }                        

                                  // dd($respondent->toArray());

        return  redirect()->route('assignment.viewResp', [$respondent->agent_id,$respondent->kuota_parameter_id] )->with('alert-success','Rubah Status Berhasil');  
        
        
    }
}
