<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Project;
use App\Models\Responden;

class ClientController extends Controller
{
    public function projects() {
    	
    	$projects = Project::where('id','!=',1)->get();

    	return view('client.project',compact('projects'));
    }

    public function enterproject($id_project)
    {
    	$project = Project::findOrFail($id_project);


    	return view('client.enterproject',compact('project'));
    }

    public function ajaxListRespByStatus (Request $request,$id_project,$id_status) 
    {
    	if($request->ajax())
    	{
    		$project = Project::findOrFail($id_project);

    		$respondens = Responden::with('status')
    								 ->where('project_id',$project->id)
    							     ->where('last_status_id',$id_status)
    							     ->get();


    		return view('client.ajaxtable',compact('project','respondens'));
    							     		
    	}
    }
}
