<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $userAuth = \Auth::user();

        if($userAuth->hasRole('admin'))
        {
            return redirect()->route('admin.dashboard');

        }elseif($userAuth->hasRole('spv'))
        {
            dd('spv');
        }elseif($userAuth->hasRole('entry'))
        {
            dd('entry');    
        }elseif($userAuth->hasRole('client'))
        {
            return redirect()->route('client.projects');
        }elseif($userAuth->hasRole('agent'))
        {
            return redirect()->route('agent.dashboard');
        }

        return view('home');

    }
}
