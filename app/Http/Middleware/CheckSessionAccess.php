<?php

namespace App\Http\Middleware;

use Closure;
use Session;

class CheckSessionAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $project = \Session::get('session_project');

        $userAuth = \Auth::user();

        if(is_null($project))
        {
            if($userAuth->hasRole(['admin','spv'])){
                return redirect()->route('admin.dashboard');
            }elseif($userAuth->hasRole(['agent'])) {
                return redirect()->route('agent.dashboard');
            }
        }

        return $next($request);
    }
}
