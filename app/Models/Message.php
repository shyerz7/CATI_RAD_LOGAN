<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = [
    	'agent_id',
    	'nama_agent',
    	'msg',
    	'is_read',
    ];

    public function agent()
    {
    	return $this->belongsTo('App\Models\Agents','agent_id');
    }
}
