<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogResponden extends Model
{
    protected $fillable = [
    	'responden_id',
    	'status_id',
    	'nomor_hp',
    	'rekaman',
        'note',
    	'tgl_reschedule',
        'agent_name'
    ];

    public function responden()
    {
    	return $this->belongsTo('App\Models\Responden','responden_id');
    }

    public function status()
    {
    	return $this->belongsTo('App\Models\Status','status_id');
    }
}
