<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Agents extends Model
{
    protected $fillable = [
    	'name',
    	'code',
    	'alamat',
    	'hp',
    	'user_id',
    	'project_id',
    ];

    public function user() 
    {
    	return $this->belongsTo('App\User','user_id');
    }

    public function respondens()
    {
        return $this->hasMany('App\Models\Responden','agent_id');
    }
}
