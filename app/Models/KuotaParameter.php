<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KuotaParameter extends Model
{
    protected $fillable = [
    	'project_id',
    	'nama_parameter',
    	'kuota',
    ];

    public function respondens () {
    	return $this->hasMany('App\Models\Responden');
    }
}
