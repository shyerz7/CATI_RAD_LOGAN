<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ListRekaman extends Model
{
	protected $table = 'list_rekaman';

    protected $fillable = [
    	'resp_id',
    	'nama_rekaman',
    	'tgl_call'
    ];
}
