<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Responden extends Model
{
    protected $fillable = [
        'project_id',
        'kuota_parameter_id',
    	'agent_id',
    	'resp_id',
    	'first_name',
    	'last_name',
    	'email',
    	'call_attempt',
    	'is_lock',
    	'nomer_hp',
    	'link_survey',
        'link_expired',
    	'last_status_id',
        'agent_name',
    ];

    public function agent()
    {
        return $this->belongsTo('App\Models\Agents','agent_id');
    }

    public function status()
    {
    	return $this->belongsTo('App\Models\Status','last_status_id');
    }

    public function kuota() 
    {
        return $this->belongsTo('App\Models\KuotaParameter','kuota_parameter_id');
    }

    public function rekaman()
    {
        return $this->hasMany('App\Models\ListRekaman','resp_id');
    }
}
