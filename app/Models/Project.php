<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = [
    	'name',
    	'target_responden',
    	'client_name',
    	'project_manajer',
    ];
}
