<?php
/**
 * admin access
 */
Route::group(['prefix'=>'api','middleware'=>['role:agent','checkaccess'] ], function(){

	Route::post('updateLockRespActive/{id_resp}',[
	    'as'    =>  'api.updateLockRespActive',
	    'uses'  =>  'AgentController@updateLockRespActive'
	]);

	Route::post('updateLockRespInActive/{id_resp}',[
	    'as'    =>  'api.updateLockRespInActive',
	    'uses'  =>  'AgentController@updateLockRespInActive'
	]);		

	

});