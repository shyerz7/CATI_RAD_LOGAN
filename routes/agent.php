<?php
Route::get('/perolehanRespByAgent/{id_agent}','AgentController@perolehanRespByAgent')->name('enterproject.perolehanRespByAgent');

Route::group(['prefix' => 'agent', 'middleware' => ['auth','role:agent']], function() {

	Route::get('dashboard',[
	    'as'    =>  'agent.dashboard',
	    'uses'  =>  'AgentController@dashboard'
	]);

	Route::get('enterproject/{id_project}',[
	    'as'    =>  'enterproject',
	    'uses'  =>  'AgentController@enterproject'
	]);

	/**
	 * admin access
	 */
	Route::group(['prefix'=>'access','middleware'=>['role:agent','checkaccess'] ], function(){

		Route::get('showdetailrep/{id_resp}',[
		    'as'    =>  'showdetailrep',
		    'uses'  =>  'AgentController@showdetailrep'
		]);

		Route::post('updateStatRespOnDetailPage/{id_resp}',[
			'as'    =>  'updateStatRespOnDetailPage',
		    'uses'  =>  'AgentController@updateStatRespOnDetailPage'
		]);

		Route::get('onsurvey/{id_resp}',[
			'as'    =>  'onsurvey',
		    'uses'  =>  'AgentController@onsurvey'
		]);

		Route::post('onsurvey/process{id_resp}',[
			'as'    =>  'onsurvey.process',
		    'uses'  =>  'AgentController@onsurveyProcess'
		]);

		Route::get('ajaxCheckKuota/{id_responden}',[
			'as'    =>  'ajaxCheckKuota',
		    'uses'  =>  'AgentController@ajaxCheckKuota'
		]);

		Route::post('messages/{id_agent}',[
			'as'    =>  'messages',
		    'uses'  =>  'AgentController@sendMessage'
		]);

	});



});
