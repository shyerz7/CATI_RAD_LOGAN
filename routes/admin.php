<?php

Route::group(['namespace' => 'Backend','prefix' => 'admin', 'middleware' => ['auth','role:admin']], function() {



	Route::get('dashboard',[
	    'as'    =>  'admin.dashboard',
	    'uses'  =>  'DashboardController@dashboard'
	]);

	Route::get('dashboard/redirect/{id_project}',[
	    'as'    =>  'admin.dashboard.redirect',
	    'uses'  =>  'DashboardController@redirect'
	]);


	/**
	 * crud project
	 */
	Route::resource('projects','ProjectController');

	/**
	 * admin access
	 */
	Route::group(['prefix'=>'access','middleware'=>['role:admin','checkaccess'] ], function(){

		/**
		 * crud agent
		 */
		Route::resource('agents','AgentController');

		/**
		 * crud status
		 */
		Route::resource('status','StatusController');

		/**
		 * crud status
		 */
		Route::resource('kuotas','KuotaParameterController');
		/**
		 * template excel untuk responden
		 */
		Route::get('/kuotasGenerateExcelTemplate/','KuotaParameterController@generateExcelTemplate')->name('kuotas.generateexcel');
		Route::post('/kuotasImportExcel','KuotaParameterController@importExcel')->name('admin.kuotas.importExcel');

		/**
		 * crud respondens
		 */
		Route::resource('respondens','RespondenController');

		/**
		 * template excel untuk responden
		 */
		Route::get('/respondensGenerateExcelTemplate/','RespondenController@generateExcelTemplate')->name('respondens.generateexcel');

		/**
		 * import data staff dari excel
		 */
		Route::post('/respondensImportExcel','RespondenController@importExcel')->name('admin.respondens.importExcel');

		/**
		 * for assignment
		 */
		Route::get('/assignment/kuota','AssignmentController@kuota')->name('assignment.kuota');
		Route::get('/assignment/{id_kuota}','AssignmentController@index')->name('assignment.index');
		Route::get('/assignment/{id_agent}/{id_kuota}','AssignmentController@assign')->name('assignment.assign');
		Route::post('/assignment/{id_agent}/{id_kuota}','AssignmentController@assignProcess')->name('assignment.assignProcess');

		Route::get('/assignment/transfer/{id_agent}/{id_kuota}','AssignmentController@assignTransfer')->name('assignment.assignTransfer');
		Route::post('/assignment/transferProcess/{id_agent}/{id_kuota}','AssignmentController@transferProcess')->name('assignment.transferProcess');

		/**
		 * view responden
		 */
		Route::get('/assignment/viewResp/{id_agent}/{id_kuota}','AssignmentController@viewResp')->name('assignment.viewResp');
		Route::get('/assignment/viewRespDetail/{id_agent}/{id_kuota}/{id_resp}','AssignmentController@viewRespDetail')->name('assignment.viewRespDetail');
		Route::put('/assignment/viewRespDetailUpdate/{id_resp}','AssignmentController@viewRespDetailUpdate')->name('assignment.viewRespDetailUpdate');

		/**
		 * tabel perolehan
		 */
		Route::get('/perolehan/','PerolehanController@index')->name('perolehan.index');

		Route::get('/perolehanRespByKuota/{id_kuota}','PerolehanController@perolehanRespByKuota')->name('perolehan.perolehanRespByKuota');
		Route::get('/perolehanRespByStatus/{id_status}','PerolehanController@perolehanRespByStatus')->name('perolehan.perolehanRespByStatus');
		Route::get('/perolehanRespByAgent/{id_agent}','PerolehanController@perolehanRespByAgent')->name('perolehan.perolehanRespByAgent');
		//perolehan ajax
		Route::get('/perolehanRespByDate/','PerolehanController@perolehanRespByDate')->name('perolehan.perolehanRespByDate');
		Route::post('/perolehanRespByDateExport/','PerolehanController@perolehanRespByDateExport')->name('perolehan.perolehanRespByDateExport');

		// list rekaman
		Route::resource('listrekaman','ListRekamanController');

		Route::get('generateExcelTemplate/','ListRekamanController@generateExcelTemplate')->name('listrekaman.generateExcel');
		Route::post('importExcel/','ListRekamanController@importExcel')->name('listrekaman.importExcel');

		Route::get('ajaxListRespByStatus/{id_project}/{id_status}',[
		    'as'    =>  'admin.ajaxListRespByStatus',
		    'uses'  =>  'ListRekamanController@ajaxListRespByStatus'
		]);

		Route::resource('pesan','MessageController');

        /**
         * perolehan
         */
        Route::get('/perolehanbyTanggal','PerolehanController@perolehanByTanggal')->name('perolehan.byTanggal');

        Route::get('/perolehanbyKuota','PerolehanController@perolehanbyKuota')->name('perolehan.byKuota');

        Route::get('/perolehanbyStatus','PerolehanController@perolehanbyStatus')->name('perolehan.byStatus');

        Route::get('/perolehanbyAgent','PerolehanController@perolehanbyAgent')->name('perolehan.byAgent');


	});

	Route::get('respondens',[
	    'as'    =>  'api.respondens',
	    'uses'  =>  'RespondenController@respondensApi'
	]);

	Route::get('exportRespondens',[
	    'as'    =>  'exportRespondens',
	    'uses'  =>  'RespondenController@exportRespondens'
	]);

});
