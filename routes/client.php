<?php

Route::group(['prefix' => 'client', 'middleware' => ['auth','role:client']], function() {

	Route::get('projects',[
	    'as'    =>  'client.projects',
	    'uses'  =>  'ClientController@projects'
	]);
	Route::get('enterproject/{id_project}',[
	    'as'    =>  'client.enterproject',
	    'uses'  =>  'ClientController@enterproject'
	]);
	Route::get('ajaxListRespByStatus/{id_project}/{id_status}',[
	    'as'    =>  'client.ajaxListRespByStatus',
	    'uses'  =>  'ClientController@ajaxListRespByStatus'
	]);

});