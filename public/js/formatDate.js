// date picker
$('.daterange').daterangepicker(
    null,
    function(start, end, label) {
        $('.daterangestart').val(start.format('YYYY-MM-DD'))
        $('.daterangeend').val(end.format('YYYY-MM-DD'))
    }
);