<?php

use Illuminate\Database\Seeder;
use App\Models\Responden;
use App\Models\KuotaParameter;


class RespondenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker 			= \Faker\Factory::create();
        $project_id = 1;

        $kuotas = KuotaParameter::get();

        foreach ($kuotas as $kuota) {

            for($i=1;$i<=10;$i++) {
                $data['kuota_parameter_id']     =   $kuota->id;
                $data['project_id']     =   $project_id;
                $data['resp_id']        =   substr( md5(rand()), 0, 7);;
                $data['first_name']     =   $faker->firstName;
                $data['last_name']      =   $faker->lastName;
                $data['email']          =   $faker->email;
                $data['link_survey']    =   'https://au1.qualtrics.com/jfe/preview/SV_8A3bq9AVWTT7qbH?Q_CHL=preview';
                $data['link_expired']   =   $faker->date($format = 'Y-m-d', $max = 'now'); 
                $data['last_status_id'] =    99;

                $array_nomer_hp = [];
                array_push($array_nomer_hp,'081234560526');
                array_push($array_nomer_hp,'087289278990');
                $data['nomer_hp']       = json_encode($array_nomer_hp);

                $responden = Responden::create($data);
            }
        }

        
    }
}
