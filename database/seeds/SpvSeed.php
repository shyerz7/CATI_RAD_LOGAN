<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;

class SpvSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = Role::findOrFail(1); //admin

        // Membuat sample admin
		$spv1 = new User();
		$spv1->name = 'SPV1';
		$spv1->email = 'spv1@rad-research.com';
		$spv1->password = bcrypt('123');
		$spv1->save();
		$spv1->attachRole($admin);

		$spv2 = new User();
		$spv2->name = 'SPV2';
		$spv2->email = 'spv2@rad-research.com';
		$spv2->password = bcrypt('123');
		$spv2->save();
		$spv2->attachRole($admin);

		$spv3 = new User();
		$spv3->name = 'SPV3';
		$spv3->email = 'spv3@rad-research.com';
		$spv3->password = bcrypt('123');
		$spv3->save();
		$spv3->attachRole($admin);

		$spv4 = new User();
		$spv4->name = 'SPV4';
		$spv4->email = 'spv4@rad-research.com';
		$spv4->password = bcrypt('123');
		$spv4->save();
		$spv4->attachRole($admin);

		$spv5 = new User();
		$spv5->name = 'SPV5';
		$spv5->email = 'spv5@rad-research.com';
		$spv5->password = bcrypt('123');
		$spv5->save();
		$spv5->attachRole($admin);
    }
}
