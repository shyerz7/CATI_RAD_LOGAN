<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Membuat role admin sistem
		$adminSistem = new Role();
		$adminSistem->name = "admin";
		$adminSistem->display_name = "Admin Sistem";
		$adminSistem->save();

		// Membuat sample admin
		$admin = new User();
		$admin->name = 'Admin Sistem';
		$admin->email = 'admin@rad-research.com';
		$admin->password = bcrypt('123');
		$admin->save();
		$admin->attachRole($adminSistem);

		// Membuat role spv sistem
		$spvSistem = new Role();
		$spvSistem->name = "spv";
		$spvSistem->display_name = "SPV Sistem";
		$spvSistem->save();

		// Membuat sample spv
		$spv = new User();
		$spv->name = 'SPV Sistem';
		$spv->email = 'spv1@rad-research.com';
		$spv->password = bcrypt('123');
		$spv->save();
		$spv->attachRole($spvSistem);

		// Membuat role entry sistem
		$entrySistem = new Role();
		$entrySistem->name = "entry";
		$entrySistem->display_name = "Entry Sistem";
		$entrySistem->save();

		// Membuat sample spv
		$entry = new User();
		$entry->name = 'Entry Sistem';
		$entry->email = 'entry1@rad-research.com';
		$entry->password = bcrypt('123');
		$entry->save();
		$entry->attachRole($entrySistem);

		// Membuat role client
		$clientSistem = new Role();
		$clientSistem->name = "client";
		$clientSistem->display_name = "Client Sistem";
		$clientSistem->save();

		// Membuat sample spv
		$client = new User();
		$client->name = 'Client Sistem';
		$client->email = 'client1@nielse.com';
		$client->password = bcrypt('123');
		$client->save();
		$client->attachRole($clientSistem);

		



		
    }
}
