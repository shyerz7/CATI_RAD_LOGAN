<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogRespondensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_respondens', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('responden_id');
            $table->unsignedInteger('status_id');
            $table->string('nomor_hp')->nullable();
            $table->string('rekaman')->nullable();
            $table->text('note')->nullable();
            $table->dateTime('tgl_reschedule')->nullable();
            $table->timestamps();

            $table->foreign('responden_id')->references('id')->on('respondens')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('status_id')->references('id')->on('statuses')
                ->onUpdate('cascade')->onDelete('cascade');    

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_respondens');
    }
}
