<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRespondensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('respondens', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('project_id');
            $table->unsignedInteger('agent_id');
            $table->string('resp_id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email');
            $table->integer('call_attempt')->nullable();
            $table->boolean('is_lock')->default(false);
            $table->string('nomer_hp'); //json
            $table->string('link_survey')->nullable();
            $table->date('link_expired')->nullable();
            $table->unsignedInteger('last_status_id')->nullable();
            $table->timestamps();

            $table->foreign('project_id')->references('id')->on('projects')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('agent_id')->references('id')->on('agents')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('last_status_id')->references('id')->on('statuses')
                ->onUpdate('cascade')->onDelete('cascade');       
                
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('respondens');
    }
}
