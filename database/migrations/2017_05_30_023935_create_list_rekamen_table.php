<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListRekamenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('list_rekaman', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('resp_id');
            $table->string('nama_rekaman');             
            $table->dateTime('tgl_call');
            $table->timestamps();
            $table->foreign('resp_id')->references('id')->on('respondens')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('list_rekaman');
    }
}
